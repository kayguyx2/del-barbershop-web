# Barbershop System

Barbershop System making with React nodeJS express graphql

## Installation

Use clone [git](https://gitlab.com/kayguyx2/del-barbershop-web.git) to install Barbershop System.

Install Server Side
```bash
Open cmd. #Command Prompt
cd /del-barbershop-web/BackEnd
npm install
```

Install Client Side
```bash
Open cmd. #Command Prompt
cd /del-barbershop-web/frontend
npm install
```

## Usage

Install Server Side
```bash
Open cmd. #Command Prompt
cd /del-barbershop-web/BackEnd
npm start
```

http://localhost:8000/graphql?

Install Client Side
```bash
Open cmd. #Command Prompt
cd /del-barbershop-web/frontend
npm start
```

http://localhost:3000


## Access in Owner and Admin

Admin
```bash
username: scarlet@admin.com
password: 132546
```

***you can create admin by owner id 

Owner
```bash
username: getto@admin.com
password: 132546
```


## License
[Phychopas](https://www.facebook.com/nay.thanet)