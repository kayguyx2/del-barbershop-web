const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  telephonenumber: {
    type: String,
    required: true
  },
  permissions: {
    type: Number,
    required: true,
    default: 1
  },
  imageUrl: {
    type: String,
    default: ''
  },
  contact: {
    type: String,
    default: ''
  },
  content: {
    type: String,
    default: ''
  },
  createdEvents: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Event'
    }
  ],
  createdService: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Service'
    }
  ],
});

module.exports = mongoose.model('User', userSchema);
