const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const servicesSchema = new Schema({
  generation: {
    type: String,
    required: true
  },
  haircut: {
    type: Boolean,
    required: true
  },
  shave: {
    type: Boolean,
    required: true
  },
  totalprice: {
    type: Number,
    required: true
  },
  creator: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
    }
},{ timestamps: true }
);

module.exports = mongoose.model('Services', servicesSchema);
