const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const priceServiceSchema = new Schema({
  cutman: {
    type: Number,
    required: true
  },
  cutstudent: {
    type: Number,
    required: true
  },
  shave: {
    type: Number,
    required: true
  }
},{ timestamps: true });

module.exports = mongoose.model('PriceService', priceServiceSchema);
