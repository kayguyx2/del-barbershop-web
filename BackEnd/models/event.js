
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const eventSchema = new Schema({

  date: {
    type: Date,
    required: true
  },
  time: {
    type: Date,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  key: {
    type: String,
    required: true
  },
  barber: {
      type: Schema.Types.ObjectId,
      ref: 'User'
  },
  user: {
      type: Schema.Types.ObjectId,
      ref: 'User'
  }
},{ timestamps: true });

module.exports = mongoose.model('Event', eventSchema);
