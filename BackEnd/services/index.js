const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'yourmail@gmail.com', // your email
      pass: 'password' // your email password
    }
  });

  let mailOptions = {
    from: 'sender@hotmail.com',                // sender
    to: 'receiver@hotmail.com',                // list of receivers
    subject: 'Hello from sender',              // Mail subject
    html: '<b>Do you receive this mail?</b>'   // HTML body
  };