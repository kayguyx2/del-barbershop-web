const Event = require('../../models/event');
const User = require('../../models/user');
var rand = require("generate-key");


module.exports = {
  createEvent: async function({ eventInput }, req) {

    const barber = await User.findById(eventInput.barber);
    if (!barber) {
      const error = new Error('Invalid barber.');
      error.code = 432;
      throw error;
    }
    
    const user = await User.findById(eventInput.userId);

    const event = new Event({
      date: eventInput.date,
      time: eventInput.time,
      name: eventInput.name,
      phone: eventInput.phone,
      email: eventInput.email,
      key: rand.generateKey(5),
      barber: barber,
      user: user
    });
    event.date.toISOString();
    event.time.toISOString();

    const createEvent = await event.save();


    barber.createdEvents.push(createEvent);
    await barber.save();

    if(user){
      user.createdEvents.push(createEvent);
      await user.save();
    }

    return { 
        ...createEvent._doc, 
        time: createEvent.time.toISOString(),
        _id: createEvent._id.toString(),
        createdAt: createEvent.createdAt.toISOString(),
        updatedAt: createEvent.updatedAt.toISOString()
    };

  },
  events: async function( {}, req){

    const event = await Event.find().populate('barber');
    if (!event) {
      const error = new Error('No user event!');
      error.code = 404;
      throw error;
    }
    return event.map( e => {
      return {...e._doc,
              date: e.date.toISOString(),
              time: e.time.toISOString(),
              _id: e._id.toString()
            }
    });
  },
  eventsByKey: async function( {key}, req){

    const event = await Event.find({key: key}).populate('creator').populate('barber');
    if (event.length === 0) {
      const error = new Error('key not correct!');
      error.code = 402;
      throw error;
    }
    return event.map( e => {
      return {
              ...e._doc,
              time: e.time.toISOString(),
              _id: e._id.toString()
            }
    });
  },
  cancelEvents: async function( {key}, req){

    const event = await Event.find({key: key}).populate('barber').populate('user');
    if (event.length === 0) {
      const error = new Error('key not correct!');
      error.code = 402;
      throw error;
    }
    
    await Event.findByIdAndRemove(event[0]._id);
    const id = event[0].barber._id;
    
    const user = event[0].user;  
    const barber = await User.findById(id);
    barber.createdEvents.pull(event[0]._id);
    await barber.save();

    if(user !== null){

      console.log("Run");
      const usid = event[0].user._id;
      const user = await User.findById(usid);
      user.createdEvents.pull(event[0]._id);
      await user.save();
    }


    return true
  },
  callByDate: async function ({ dateIn }, req) {
    const event = await Event.find()
      .where('date', dateIn)
      .populate('createdEvents');

    if (!event) {
      const error = new Error('Event not found.');
      error.code = 401;
      throw error;
    }
    return event.map(e => {
      return {
        time: e.time.toISOString(),
        _id: e._id.toString(),
      }
    })
  }
};