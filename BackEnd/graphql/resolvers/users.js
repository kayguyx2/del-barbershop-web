const User = require('../../models/user');

const { transformUser } = require('./merge');

module.exports = {
  users: async ( {} , req) => {
    try {
      
      const users = await User.find({$or: [{'permissions': 1}, {'permissions': 0}]});
      return users.map(user => {
        return transformUser(user);
      });
    } catch (err) {
      throw err;
    }
  }
};
