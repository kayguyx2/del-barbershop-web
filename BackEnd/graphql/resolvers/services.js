const User = require('../../models/user');
const Services = require('../../models/services');
const PriceService = require('../../models/priceservice');
module.exports = {

  createServices: async function ({ servicesInput }, req) {

    // login หรือไม่
    if (!req.isAuth) {
      const error = new Error('Not authenticated!');
      error.code = 401;
      throw error;
    }
    // ตรวจสอบ user
    const user = await User.findById(req.userId);
    if (!user) {
      const error = new Error('Invalid user.');
      error.code = 401;
      throw error;
    }


    const services = new Services({
      generation: servicesInput.generation,
      haircut: servicesInput.haircut,
      shave: servicesInput.shave,
      totalprice: servicesInput.totalprice,
      creator: user
    });
    const createdServices = await services.save();

    user.createdService.push(createdServices);
    await user.save();

    return {
      ...createdServices._doc,
      _id: createdServices._id.toString(),
      createdAt: createdServices.createdAt.toISOString(),
      updatedAt: createdServices.updatedAt.toISOString()
    };
  },
  service: async function ({ id }, req) {
    if (!req.isAuth) {
      const error = new Error('Not authenticated!');
      error.code = 401;
      throw error;
    }
    const service = await Services.findById(id).populate('creator');
    if (!service) {
      const error = new Error('No post found!');
      error.code = 404;
      throw error;
    }
    return {
      ...service._doc,
      _id: service._id.toString(),
      createdAt: service.createdAt.toISOString(),
      updatedAt: service.updatedAt.toISOString()
    };
  },
  services: async function ({ }, req) {

    if (!req.isAuth) {
      const error = new Error('Not authenticated!');
      error.code = 401;
      throw error;
    }

    const totalservice = await Services.find().countDocuments();
    const services = await Services.find()
      .sort({ createdAt: -1 })
      .populate('creator');
    return {
      services: services.map(ser => {
        return {
          ...ser._doc,
          _id: ser._id.toString(),
          createdAt: ser.createdAt.toISOString(),
          updatedAt: ser.updatedAt.toISOString()
        };
      }),
      totalservice: totalservice
    };
  },
  delService: async function ({ serviceID }, req) {

    if (!req.isAuth) {
      const error = new Error('Not authenticated!');
      error.code = 401;
      throw error;
    }
    const ser = await Services.findById(serviceID).populate('creator');
    await Services.findByIdAndRemove(serviceID);

    const barber = await User.findById(ser.creator._id);
    barber.createdService.pull(serviceID);
    console.log("barber" ,barber);
    
    await barber.save();
    return true


  },
  createPriceServic: async function ({ priceInput }, req) {

    // if (!req.isAuth) {
    //   const error = new Error('Not authenticated!');
    //   error.code = 401;
    //   throw error;
    // }

    const priceservice = new PriceService({
      cutman: priceInput.cutman,
      cutstudent: priceInput.cutstudent,
      shave: priceInput.shave,
    });

    const createPriceServic = await priceservice.save();
    return {
      ...createPriceServic._doc,
      _id: createPriceServic._id.toString(),
      createdAt: createPriceServic.createdAt.toISOString(),
      updatedAt: createPriceServic.updatedAt.toISOString()
    };
  },
  callPriceServic: async function ({  }, req) {

    // if (!req.isAuth) {
    //   const error = new Error('Not authenticated!');
    //   error.code = 401;
    //   throw error;
    // }

    const priceservice = await PriceService.findById("5d57b9c787834839945100a2");
    // console.log(priceservice);
    
    return {
      ...priceservice._doc,
      _id: priceservice._id.toString(),
      createdAt: priceservice.createdAt.toISOString(),
      updatedAt: priceservice.updatedAt.toISOString()
    };
  },
  updatePriceService: async function({ priceInput }, req) {

    // if (!req.isAuth) {
    //   const error = new Error('Not authenticated!');
    //   error.code = 401;
    //   throw error;
    // }

    const priceservice = await PriceService.findById("5d57b9c787834839945100a2");
    if (!priceservice) {
      const error = new Error('No post found!');
      error.code = 404;
      throw error;
    }
    priceservice.cutman = priceInput.cutman;
    priceservice.cutstudent = priceInput.cutstudent;
    priceservice.shave = priceInput.shave;

    const updatedPriceService = await priceservice.save();
    return {
      ...updatedPriceService._doc,
      _id: updatedPriceService._id.toString(),
      createdAt: updatedPriceService.createdAt.toISOString(),
      updatedAt: updatedPriceService.updatedAt.toISOString()
    };
  }
};