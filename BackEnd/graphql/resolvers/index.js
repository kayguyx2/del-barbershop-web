const authResolver = require('./auth');
const eventsResolver = require('./events');
const bookingResolver = require('./booking');
const userResolver = require('./users');
const servicesResolver = require('./services');

const rootResolver = {
  ...authResolver,
  ...eventsResolver,
  ...bookingResolver,
  ...userResolver,
  ...servicesResolver
};

module.exports = rootResolver;
