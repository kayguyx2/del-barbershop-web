const fs = require('fs');
const path = require('path');

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const validator = require('validator');
const User = require('../../models/user');

// forgot 

const { sendMail } = require("../../services/index");

module.exports = {
  forgotPassword: async function ({ email }) {
    const user = await User.findOne({ email: email });

    if (!user) throw new Error("There is no user with this email");

    const token = jwt.sign(
      { userId: user.id, email: user.email },
      'somesupersecretkey',
      {
        expiresIn: '1h'
      }
    );

    await user.save();
    await sendMail(user.email, token);

    return "Reset password token sended successfully please check your email";
  },
  callUser: async function ({ userId }, req) {
    const user = await User.findById(userId).populate('createdEvents');
    if (!user) {
      const error = new Error('User not found.');
      error.code = 401;
      throw error;
    }
    return {
      ...user._doc,
      _id: user._id.toString(),
    }
  },
  updateUser: async function ({ updateUserInput }, req) {


    const user = await User.findById(updateUserInput._id);
    if (!user) {
      const error = new Error('User not found.');
      error.code = 401;
      throw error;
    }

    user.name = updateUserInput.name;
    user.telephonenumber = updateUserInput.telephonenumber;
    user.content = updateUserInput.content;
    user.contact = updateUserInput.contact;
    const updatedUser = await user.save();
    return {
      ...updatedUser._doc,
      _id: updatedUser._id.toString()
    };
  },
  blockUser: async function ({ userID }, req) {

    const user = await User.findById(userID);
    if (!user) {
      const error = new Error('User not found.');
      error.code = 401;
      throw error;
    }

    user.permissions = 0

    const updatedUser = await user.save();
    return true
  },
  unblockUser: async function ({ userID }, req) {

    const user = await User.findById(userID);
    if (!user) {
      const error = new Error('User not found.');
      error.code = 401;
      throw error;
    }

    user.permissions = 1

    const updatedUser = await user.save();
    return true
  },
  callAdmin: async function ({ }, req) {
    const user = await User.find()
      .where('permissions', 2)
      .populate('createdEvents');

    if (!user) {
      const error = new Error('User not found.');
      error.code = 401;
      throw error;
    }
    return user.map(user => {
      return {
        ...user._doc,
        _id: user._id.toString(),
      }
    })
  },
  delUser: async function ({ userInput }, req) {
    if (!req.isAuth) {
      const error = new Error('Not authenticated!');
      error.code = 401;
      throw error;
    }
    const user = await User.findById(userInput);
    if (!user) {
      const error = new Error('incorect userInput');
      error.code = 404;
      throw error;
    }
    await User.findByIdAndRemove(userInput);
    return true
  },
  createUser: async function ({ userInput }, req) {
    if (!validator.isEmail(userInput.email)) {
      const error = new Error('incorect email');
      error.code = 426;
      throw error;
    }
    if (
      validator.isEmpty(userInput.password) ||
      !validator.isLength(userInput.password, { min: 5 })
    ) {
      const error = new Error('Password too short!');
      error.code = 425;
      throw error;
    }
    if (
      validator.isEmpty(userInput.telephonenumber) ||
      !validator.isLength(userInput.telephonenumber, { min: 10, max: 10 })
    ) {
      const error = new Error('telephonenumber too short!');
      error.code = 427;
      throw error;
    }
    const existingUser = await User.findOne({ email: userInput.email });
    if (existingUser) {
      const error = new Error('User exists already!');
      error.code = 423;
      throw error;
    }
    const hashedPassword = await bcrypt.hash(userInput.password, 12);

    const user = new User({
      name: userInput.name,
      telephonenumber: userInput.telephonenumber,
      email: userInput.email,
      password: hashedPassword
    });

    const createdUser = await user.save();
    return { ...createdUser._doc, _id: createdUser._id.toString() };
  },
  createAdmin: async function ({ userInput }, req) {
    if (!validator.isEmail(userInput.email)) {
      const error = new Error('incorect email');
      error.code = 426;
      throw error;
    }
    if (
      validator.isEmpty(userInput.password) ||
      !validator.isLength(userInput.password, { min: 5 })
    ) {
      const error = new Error('Password too short!');
      error.code = 425;
      throw error;
    }
    // if (
    //   validator.isEmpty(userInput.telephonenumber) ||
    //   !validator.isLength(userInput.telephonenumber, { min: 10, max: 10 })
    // ) {
    //   const error = new Error('telephonenumber too short!');
    //   error.code = 427;
    //   throw error;
    // }
    const existingUser = await User.findOne({ email: userInput.email });
    if (existingUser) {
      const error = new Error('User exists already!');
      error.code = 423;
      throw error;
    }
    const hashedPassword = await bcrypt.hash(userInput.password, 12);

    const user = new User({
      name: userInput.name,
      telephonenumber: userInput.telephonenumber,
      email: userInput.email,
      password: hashedPassword,
      imageUrl: userInput.imageUrl,
      contact: userInput.contact,
      content: userInput.content,
      permissions: userInput.permissions
    });

    const createdUser = await user.save();
    return { ...createdUser._doc, _id: createdUser._id.toString() };
  },
  login: async function ({ email, password }) {

    if (
      validator.isEmpty(email) ||
      validator.isEmpty(password)
    ) {
      const error = new Error('isEmpty Data');
      error.code = 403;
      throw error;
    }
    const user = await User.findOne({ email: email });
    if (!user) {
      const error = new Error('User not found.');
      error.code = 401;
      throw error;
    }
    const isEqual = await bcrypt.compare(password, user.password);
    if (!isEqual) {
      const error = new Error('Password is incorrect.');
      error.code = 402;
      throw error;
    }

    const token = jwt.sign(
      { userId: user.id, email: user.email },
      'somesupersecretkey',
      {
        expiresIn: '1h'
      }
    );
    return { userId: user.id, token: token, permissionCode: user.permissions };
  }
};
