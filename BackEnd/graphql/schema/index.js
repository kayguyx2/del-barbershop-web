const { buildSchema } = require('graphql');

module.exports = buildSchema(`
type Booking {
    _id: ID!
    event: Event!
    user: User!
    createdAt: String!
    updatedAt: String!
}

type Event {
  _id: ID!
  date: String!
  time: String!
  name: String!
  phone: String!
  email: String!
  key: String!
  createdAt: String!
  updatedAt: String!
  barber: User!
  user: User
}

type PriceService {
  _id: ID!
  cutman: Int!
  cutstudent: Int!
  shave: Int
  createdAt: String!
  updatedAt: String!
}

type User {
  _id: ID!
  name: String!
  telephonenumber: String!
  email: String!
  contact: String
  content: String
  imageUrl: String
  permissions: Int!
  password: String
  createdEvents: [Event!]!
  createdService: [Services!]!
}

type Services {
  _id: ID!
  generation: String!
  haircut: Boolean!
  shave: Boolean!
  totalprice: String!
  createdAt: String!
  updatedAt: String!
  creator: User!
}

type AuthData {
  userId: ID!
  token: String!
  permissionCode: Int!
}

type serviceData {
  services: [Services!]!
  totalservice: Int!
}

input ServicesInput {
  generation: String!
  haircut: Boolean!
  shave: Boolean!
  totalprice: Int!
}
input PriceInput {
  cutman: Int!
  cutstudent: Int!
  shave: Int!
}

input EventInput {
  date: String!
  time: String!
  name: String!
  phone: String!
  email: String!
  barber: String!
  userId: String
}


input UserInput {
  name: String!
  telephonenumber: String!
  email: String!
  password: String!
}
input UpdateUserInput {
  _id: String! 
  name: String!
  telephonenumber: String!
  content: String
  contact: String
}
input BlockUserInput {
  _id: String! 
  premissions: Int!
}

input UserInputAdmin {
  name: String!
  telephonenumber: String!
  email: String!
  contact: String
  content: String
  imageUrl: String
  permissions: Int!
  password: String!
}
type RootQuery {
    users: [User!]!
    service(id: ID!): Services!
    services: serviceData!
    events: [Event!]!
    eventsByKey( key: String!): [Event!]!
    bookings: [Booking!]!
    login(email: String!, password: String!): AuthData!
    callUser(userId: ID!): User!
    callAdmin: [User!]!
    callByDate( dateIn: String!): [Event!]
    callPriceServic : PriceService
}

type RootMutation {
    createPriceServic(priceInput: PriceInput): PriceService
    updatePriceService(priceInput: PriceInput): PriceService!
    updateUser(updateUserInput: UpdateUserInput): User!
    blockUser(userID: ID!): Boolean
    delService(serviceID: ID!): Boolean
    delUser(userInput: ID!): Boolean
    unblockUser(userID: ID!): Boolean
    forgotPassword(email: String!): String!
    changePassword(token: String!, password: String!): User!
    createServices(servicesInput: ServicesInput): Services!
    createEvent(eventInput: EventInput): Event
    createUser(userInput: UserInput): User
    createAdmin(userInput: UserInputAdmin): User
    bookEvent(eventId: ID!): Booking!
    cancelBooking(bookingId: ID!): Event!
    cancelEvents(key: String!): Boolean
    
}

schema {
    query: RootQuery
    mutation: RootMutation
}
`);
