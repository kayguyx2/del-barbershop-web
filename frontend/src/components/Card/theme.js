export default ({ spacing }) => ({
    MuiCard: {
      root: {
        '&.MuiEngagementCard--01': {
          transition: '0.3s',
          maxWidth: 250,
          margin: '20px auto',
          color: '#f5f5f5',
          // border: '20px solid red',
          display: 'inline-block',
          width: '100%',
          marginLeft: '15px',
          boxShadow: '0 8px 40px -12px rgba(0,0,0,0.3)',
          '&:hover': {
            boxShadow: '0 16px 70px -12.125px rgba(0,0,0,0.3)',
            color: 'red',
            opacity: 0.85,
          },
          '& .MuiCardMedia-root': {
            paddingTop: '70.25%',
          },
          '& .MuiCardContent-root': {
            textAlign: 'left',
            backgroundColor : 'rgba(0,0,0,0.8)',
            padding: spacing(1),
          },
          '& .MuiDivider-root': {
            margin: spacing(3, 0),
          },
          '& .MuiTypography--heading': {
            textAlign: 'center',
            fontWeight: 'bold',
          },
          '& .MuiTypography--subheading': {
            textAlign: 'center',
            color: 'rgb(51,153,0)'
          },
          
        },
        '&.MuiEngagementCard--02': {
          transition: '0.3s',
          maxWidth: 250,
          margin: '20px auto',
          display: 'inline-block',
          width: '100%',
          marginLeft: '15px',
          boxShadow: '0 16px 70px -12.125px rgba(0,0,0,0.3)',
          color: 'red',
          opacity: 0.7,
          '& .MuiCardMedia-root': {
            paddingTop: '70.25%',
          },
          '& .MuiCardContent-root': {
            textAlign: 'left',
            backgroundColor : 'rgba(0,0,0,0.8)',
            padding: spacing(1),
          },
          '& .MuiDivider-root': {
            margin: spacing(3, 0),
          },
          '& .MuiTypography--heading': {
            textAlign: 'center',
            fontWeight: 'bold',
          },
          '& .MuiTypography--subheading': {
            textAlign: 'center',
            color: 'rgb(51,153,0)'
          },
        },
        '&.MuiEngagementCard--03': {
          transition: '0.3s',
          maxWidth: 250,
          margin: '20px auto',
          display: 'inline-block',
          width: '100%',
          marginLeft: '15px',
          boxShadow: '0 16px 70px -12.125px rgba(0,0,0,0.3)',
          color: '#f5f5f5',
          '& .MuiCardMedia-root': {
            paddingTop: '70.25%',
          },
          '& .MuiCardContent-root': {
            textAlign: 'left',
            backgroundColor : 'rgba(0,0,0,0.8)',
            padding: spacing(1),
          },
          '& .MuiDivider-root': {
            margin: spacing(3, 0),
          },
          '& .MuiTypography--heading': {
            textAlign: 'center',
            fontWeight: 'bold',
          },
          '& .MuiTypography--subheading': {
            textAlign: 'center',
            color: 'rgb(51,153,0)'
          },
        },
      },
    },
  });