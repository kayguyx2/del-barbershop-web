import React from 'react';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';


const EngagementCard01 = (props) => (
  <Card className={props.cssName} onClick={ () => props.dis ? props.setAdminId(props):undefined}>
    <CardMedia
      className={'MuiCardMedia-root'}
      image={
        props.imges
      }
    />
    <CardContent className={'MuiCardContent-root'}>
      <Typography
        className={'MuiTypography--heading'}
        variant={'h6'}
        gutterBottom
      >
        {props.name}
      </Typography>
      
      {props.status && 
        (<Typography
          className={'MuiTypography--subheading'}
          variant={'body2'}
        >
          online
        </Typography>)}
      
      {/* <Divider className={'MuiDivider-root'} light /> */}
    </CardContent>
  </Card>
);

export default EngagementCard01;