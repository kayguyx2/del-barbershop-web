  import React from "react";
  import { createMuiTheme } from "@material-ui/core";
  import { ThemeProvider } from "@material-ui/styles";
  import EngagementCard01 from './EngagementCard01';
  import createOverrides from './theme';

  const baseTheme = createMuiTheme();
  
  const MainCard = props => (
    <ThemeProvider
      theme={{
        ...baseTheme,
        overrides: createOverrides(baseTheme)
      }}
    > 
          <EngagementCard01
            dis={props.dis}
            name={props.name} 
            imges={props.img} 
            status={props.status}
            adminId={props.adminId}
            setAdminId={props.setAdminId}
            cssName={props.cssName}
            index={props.index} />

    </ThemeProvider>
  )
  
  export default MainCard