import React from 'react';
import styled from 'styled-components';
import { Row, Col } from 'react-bootstrap';
import img from '../../../assets/scalet.jpg';

// box-shadow: 0 16px 24px 2px rgba(0, 0, 0, 0.14),
// 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2);
const Styles = styled.div`
    text-align: center;
    font-family: 'Acme', sans-serif;
    .mr-3{
        position: relative;
        border-radius: 5px;
        margin: auto;
        max-width: 450px;
        width: 100%
        height: auto;
        margin-top 20px

    }
    .form-head{  
        margin-top 20px;
    }
    .form-content-eng{
        font-family: 'Roboto', sans-serif;
    }

    hr.style-two {
        border: 0;
        height: 0.2;
        border-top: 1px solid rgba(0, 0, 0, 0.5);
        border-bottom: 1px solid rgba(255, 255, 255, 0.3);
    }
`;
const History = () => (
    <Styles>
        <Row>
            <Col sm={1}></Col>
            <Col sm={5}>
                <img
                    className="mr-3"
                    src={img}
                    alt="History img"
                />

            </Col>
            <Col sm={5}>
                <div className="form-head">
                    <h3>Our Branches</h3>
                    <hr className="style-two" />

                </div>
                <div className="form-content-eng">
                    <p>
                        ....<br />
                        We provide better male grooming services, cut and shave, shave, wash, hi light, color,
                        head spas and massage. you can expect the unexpected here.
                        <br />
                        <br />
                        ร้านตัดผมสำหรับผู้ชาย ที่สามารถตอบสนองความต้องการได้ครบ และยังสามารถสร้างความประทับใจด้วย
                        ทั้งในด้านการให้บริการ,ผลิตภัณฑ์ที่ใช้ และราคาที่สมเหตุสมผล
                    </p>
                </div>
            </Col>
            <Col sm={1}></Col>
        </Row>
    </Styles>
);

export default History;