import React from 'react';
import VideoPlayer from 'react-video-markers';

class Player extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPlaying: false
    };
  }
  

  handlePlay = () => {
    this.setState({ isPlaying: true });
  };

  handlePause = () => {
    this.setState({ isPlaying: false });
  };


  render() {
    const { isPlaying } = this.state;

    return (
      <VideoPlayer
        url="http://localhost:8000/images/vdo/barbercrew2.mp4"
        isPlaying={isPlaying}
        volume={0.5}
        onPlay={this.handlePlay}
        onPause={this.handlePause}
        onVolume={this.handleVolume}
      />
    )
  }
}

export default Player;


//ixRKLX02GIA