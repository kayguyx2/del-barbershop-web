import React, { Component } from 'react'
import styled from 'styled-components';

const Styles = styled.p`
  text-align: right;
  margin-right: 5%

`;
class Clock extends Component {
 

    constructor() {
        super()
        this.state={
            time:new Date()
          }
    }
    
      currentTime()
      {
        this.setState({
          time: new Date()
        })
      }
      componentDidMount() {
        
      }
      componentWillMount() {
        this.terval = setInterval(()=>this.currentTime(),1000);
      }
      componentWillUnmount() {
        clearInterval(this.terval)
      }
    
    
      render() {
    
        return (
          <Styles>
            <i className="far fa-clock"> : </i> {this.state.time.toLocaleTimeString()}
          </Styles>
        )
      }
}

export default Clock;