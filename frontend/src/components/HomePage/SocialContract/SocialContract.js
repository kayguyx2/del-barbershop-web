import React from 'react';
import styled from 'styled-components';

const Styles = styled.div`
    a {
        margin-left: 5%
        text-align: left;
        position: relative;
        color: inherit; 
        text-decoration: none;
        
        &:hover {
            color: Silver;
        }
    }
`;
const SocialContract = (props) => (
        <Styles>
                {props.facebook && <a 
                    className="so-fac" 
                    href="https://www.facebook.com/BARBERCREWCNX/?epa=SEARCH_BOX" 
                    rel="noopener noreferrer"
                    target='_blank'  >
                    <i className="fab fa-facebook-square" > {props.facebook}</i>
                </a>}
                {props.instagram &&<a 
                    className="so-ins" 
                    href="https://www.facebook.com/BARBERCREWCNX/?epa=SEARCH_BOX" 
                    rel="noopener noreferrer"
                    target="_blank">
                    <i className="fab fa-instagram"> {props.instagram}</i>
                </a>}
        </Styles>
        
);
            
export default SocialContract;