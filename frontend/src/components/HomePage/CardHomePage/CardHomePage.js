import React from 'react';
import { Card } from 'react-bootstrap';

const CardHomepage = props => (
    <Card>
        <Card.Body>
            {props.children}
        </Card.Body>
    </Card>
);

export default CardHomepage;