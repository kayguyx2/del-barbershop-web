import React from 'react'
import styled from 'styled-components';


const Styles = styled.div`
    
    h3 {
        text-align: center;
        color: Black;
        margin: 0; 
    }

    hr.style-eight {
        overflow: visible; 
        padding: 0;
        border: none;
        border-top: medium double #333;
        color: #333;
        text-align: center;
    }
    hr.style-eight:after {
        content: "§";
        display: inline-block;
        position: relative;
        top: -0.7em;
        font-size: 1.5em;
        padding: 0 0.25em;
        background: white;
    }


`;
const LineSubtitle = props => (
    
    <Styles>
        <hr className={props.stylez}/>
        <h3>{props.subTitle}</h3>
        <hr className={props.stylez}/>
    </Styles>
)

export default LineSubtitle