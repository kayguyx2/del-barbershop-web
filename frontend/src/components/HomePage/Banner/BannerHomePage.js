import React from 'react';
import styled from 'styled-components';
import Background from '../../../assets/bg3.jpg';

const Styles = styled.div`
    background-image: url("${Background}");
    background-size: cover;
    z-index: -1;
    max-width: 100%;
    height: 500px;
    background-repeat: no-repeat;
    background-position: center;
`;
const BannerHome = props => (
    <Styles>
        {props.children}
    </Styles>
);

export default BannerHome;
