import React from 'react';
import styled from 'styled-components';
import Background from '../../../assets/bg3.jpg';

const Styles = styled.div`
    background-image: url("${Background}");
    background-size: cover;
    max-width: 100%;
    height: 720px;
    background-repeat: no-repeat;
    background-position: center;

    .display {
        height: 300px;
        width: 480px;
    }

    @media (max-width: 575.98px) {
        height: 400px;
    }

    @media (min-width: 576px) and (max-width: 767.98px) { 
        height: 450px;
     }

    @media (min-width: 768px) and (max-width: 991.98px) { 
        height: 500px;
     }

    @media (min-width: 992px) and (max-width: 1199.98px) { 
        height: 720px;
     }
`;
const Banner = props => (
    <Styles>
        {props.children}
    </Styles>
);

export default Banner;
