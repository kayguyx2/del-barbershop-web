import React, { Component } from 'react';
import Card from '../../Card/MainCard';
import styled from 'styled-components';

// background-color: rgba(0,0,0,0.9);
// color: #f5f5f5;
const WarpperTeam = styled.div`

    text-align: center;
    font-family: 'Acme', sans-serif;

    .title{
        margin: 25px
    }
`;
export default class Team extends Component {
    state = {
        Admin: [],
        isLoading: false,
        status: true
    }
    componentDidMount() {
        this.callAdmin();
    }
    callAdmin = () => {
        this.setState({ isLoading: true });
        const requestData = {
            query: `
                query{
                    callAdmin{
                      name
                      imageUrl
                    }
                }
            `
        };
        fetch('http://localhost:8000/graphql', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestData),
        })
            .then(res => {
                return res.json();
            })
            .then(resData => {
                console.log(resData.data.callAdmin);
                this.setState({
                    Admin: resData.data.callAdmin,
                    isLoading: false
                });
            })
            .catch(err => {
                this.setState({ isLoading: false });
                console.log(err);
            });
    }

    render() {

        return (
            <WarpperTeam>
                <div className="title">
                    <h3>THE BARBERCREW BARBERS ONLINE</h3>
                    {this.state.Admin.map( (admin,index) =>( 
                        
                        <Card 
                            key={index}
                            name={admin.name}
                            cssName="MuiEngagementCard--01"
                            img={'http://localhost:8000/' + admin.imageUrl}
                            status={this.state.status}
                        />
                    ))}
                </div>
            </WarpperTeam>
        )
    }
}
