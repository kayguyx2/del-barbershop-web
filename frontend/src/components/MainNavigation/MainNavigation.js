import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Nav, Navbar, Container, NavDropdown } from 'react-bootstrap';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import './nav.css';
const Styles = styled.div`
  .navbar {
    background-color: rgba(0,0,0,0.8) !important;
    padding: 1rem 1rem;
    display: flex;
    border: 0;
    width: 100%;
    height: 10vh;
  }
  a,
  .navbar-nav .nav-link  {
    
    color: Ivory;
    text-decoration: none;
    padding: 0 1rem;

    &:hover {
      color: #bbb;
    }
  }

  .nav-dropdows-select{
    color: rgba(0,0,0,0.8);
    &:hover {
      color: silver;
    }
  }

  .nav-dropdows{

    color: Ivory;
    text-decoration: none;
    &:hover {
      color: #bbb;
    }
  }
  .navbar-brand {
    pointer-events: none;
  }
  .ml-auto {
    margin-top: 3px;
  }
  .navbar-toggler {
    background: #f0ffff;
  }
  .selected{
    color: red;
  }
`;

const MainNavigation = props => (

  <Styles>
    <Navbar expand="lg" >
      <Container>
        <Navbar.Brand>
          <Link to="/">BARBERSHOP</Link>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" styled={{ backgroundColor: "rgba(0,0,0,0.8)" }} />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="ml-auto">

            {/* //////// Owner Nav ////////// */}

            {(props.isAuth && (props.isPermis === 3)) && (
              <Nav.Item>
                <NavLink to="/main" exact activeClassName='selected'  >Main</NavLink>
              </Nav.Item>
            )}
            {(props.isAuth && (props.isPermis === 3)) && (
              <NavDropdown
                title="Manage"
                id="collasible-nav-dropdown"
                className="nav-dropdows"
              >
                <Nav.Item>
                  <NavLink
                    className="nav-dropdows-select"
                    to="/signup"
                    activeClassName='selected'>Create Admin
                              </NavLink>
                </Nav.Item>
                <Nav.Item>
                  <NavLink
                    className="nav-dropdows-select"
                    to="/handleadmin"
                    activeClassName='selected'>Handle Admin
                              </NavLink>
                </Nav.Item>
              </NavDropdown>
            )}

            {/* //////// Admin Nav ////////// */}
            {(props.isAuth && (props.isPermis === 2)) && (
              <Nav.Item>
                <NavLink to="/admin" exact activeClassName='selected'  >Admin</NavLink>
              </Nav.Item>
            )}
            {(props.isPermis < 2 && props.isPermis !== 0) && (
              <Nav.Item>
                <NavLink to='/home' activeClassName='selected' >Home</NavLink>
              </Nav.Item>
            )}
            {(props.isPermis < 2 && props.isPermis !== 0) && (
              <NavDropdown
                title="Booking"
                id="collasible-nav-dropdown"
                className="nav-dropdows"
              >
                <Nav.Item>
                  <NavLink
                    className="nav-dropdows-select"
                    to="/booking"
                    activeClassName='selected'>Booking
                  </NavLink>
                </Nav.Item>
                <Nav.Item>
                  <NavLink
                    className="nav-dropdows-select"
                    to="/handle"
                    activeClassName='selected'>Handle
                  </NavLink>
                </Nav.Item>
              </NavDropdown>
            )}
            {(props.isPermis < 2 && props.isPermis !== 0) && (
              <Nav.Item>
                <NavLink
                  to="/contact"
                  activeClassName='selected'>Contact Us
                </NavLink>
              </Nav.Item>
            )}
            <Nav.Item>
              {!props.isAuth && <NavLink activeClassName='selected' to="/login" >Login</NavLink>}
            </Nav.Item>
            <Nav.Item>
            {!props.isAuth && <NavLink to="/register" activeClassName='selected'>Register</NavLink>}
            </Nav.Item>
            {(props.isPermis === 1 || props.isPermis === 2) && (
              <NavDropdown
                title="Profile"
                id="collasible-nav-dropdown"
                className="nav-dropdows"
              >
                {(props.isPermis === 1) && (
                  <Nav.Item>
                    <NavLink
                      className="nav-dropdows-select"
                      to="/userinfor"
                      activeClassName='selected'>Information
                  </NavLink>
                  </Nav.Item>
                )}
                {(props.isPermis === 2) && (
                  <Nav.Item>
                    <NavLink
                      className="nav-dropdows-select"
                      to="/admininfor"
                      activeClassName='selected'>Information
                  </NavLink>
                  </Nav.Item>
                )}
                <Nav.Item>
                  <NavLink
                    className="nav-dropdows-select"
                    to="/"
                    onClick={props.onLogout}
                    >Logout
                  </NavLink>
                </Nav.Item>
              </NavDropdown>
            )}
            {(props.isPermis === 3) && (
              <Nav.Item>
                <Link to="/" onClick={props.onLogout}>
                  Logout
                  </Link>
              </Nav.Item>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>

    </Navbar>
  </Styles>
);
export default MainNavigation;

MainNavigation.propTypes = {
  color: PropTypes.string,
};