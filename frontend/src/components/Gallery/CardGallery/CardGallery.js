import React from 'react';
import Gallery from "react-photo-gallery";
import { photos } from "./photos";
import styled from 'styled-components';
import { Container } from 'react-bootstrap';

const WarpperGallery = styled.div`

    text-align: center;
    font-family: 'Acme', sans-serif;

    .title{
        padding-top: 25px;
        padding-bottom: 25px;
        color: rgba(0,0,0,0.9);
        
    }
    .vdo{
        padding-bottom: 70px;
    }
`;
const CardGallery = props => (
    <WarpperGallery>
        <div className="title">
            <h3>OUR STYLED</h3>
        </div>
        <Container>
            <Gallery photos={photos} />
            <Container>
                <br />
                <hr className="style-two" />
            </Container>
        </Container>
    </WarpperGallery>
    
);

export default CardGallery;
