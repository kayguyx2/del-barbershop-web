export const photos = [
    {
      src: "http://localhost:8000/images/styled/01.jpg",
      sizes: ["(min-width: 480px) 50vw,(min-width: 1024px) 33.3vw,100vw"],
      width: 4,
      height: 3
    },
    {
      src: "http://localhost:8000/images/styled/02.jpg",
      sizes: ["(min-width: 480px) 50vw,(min-width: 1024px) 33.3vw,100vw"],
      width: 2,
      height: 3
    },
    {
      src: "http://localhost:8000/images/styled/03.jpg",
      sizes: ["(min-width: 480px) 50vw,(min-width: 1024px) 33.3vw,100vw"],
      width: 3,
      height: 4
    },
    {
      src: "http://localhost:8000/images/styled/04.jpg",
      sizes: ["(min-width: 480px) 50vw,(min-width: 1024px) 33.3vw,100vw"],
      width: 3,
      height: 3
    },
    {
      src: "http://localhost:8000/images/styled/05.jpg",
      sizes: ["(min-width: 480px) 50vw,(min-width: 1024px) 33.3vw,100vw"],
      width: 3,
      height: 4
    },
    {
      src: "http://localhost:8000/images/styled/06.jpg",
      sizes: ["(min-width: 480px) 50vw,(min-width: 1024px) 33.3vw,100vw"],
      width: 3,
      height: 2
    }
  ];
  