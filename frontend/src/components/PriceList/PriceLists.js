import React from 'react';
import { createMuiTheme } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';
import PriceList from './Core-Price/PriceList';
import createOverrides from './Core-Price/theme';

const baseTheme = createMuiTheme();

const PriceLists = () => (
  <ThemeProvider
    theme={{
      ...baseTheme,
      overrides: createOverrides(baseTheme),
    }}
  >
    <PriceList />
    {console.log(baseTheme)}
  </ThemeProvider>
);

export default PriceLists;
