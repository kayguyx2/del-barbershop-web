import React, { Component } from 'react'
import styled from 'styled-components';
import { Form, Container } from 'react-bootstrap';
const WarpperPageTwo = styled.div`
    h3 {
        margin-top: 25px;
        font-family: 'Acme', sans-serif;
        text-shadow: 2px 2px 15px rgba(206,51,44,0.73);
        text-align: center;
    }

    .form-input{
        margin: 0px 25px;
    }
    .input-form-p3{
        padding-left: 15px;
    }
`;
class Page_3 extends Component {
    render() {
        return (
            <WarpperPageTwo>
                <h3>User information</h3>
                <hr />
                <Container>
                    <Form className="form-input">
                        <Form.Group>
                            <Form.Label>name</Form.Label>
                            <Form.Control
                                type="text"
                                name="name"
                                value={this.props.name}
                                onChange={this.props.handleChange}
                                onBlur={this.props.handlevalid}
                                isInvalid={this.props.validname}
                            >
                            </Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>phone</Form.Label>
                            <Form.Control
                                type="text"
                                name="phone"
                                value={this.props.phone}
                                onChange={this.props.handleChange}
                                onBlur={this.props.handlevalid}
                                isInvalid={this.props.validphone}
                            >
                            </Form.Control>
                            <Form.Text className="text-muted">
                                Phone number that can be contacted.
                        </Form.Text>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>email</Form.Label>
                            <Form.Control
                                type="email"
                                name="email"
                                value={this.props.email}
                                onChange={this.props.handleChange}
                                onBlur={this.props.handlevalid}
                                isInvalid={this.props.validemail}
                            >
                            </Form.Control>
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                        </Form.Text>
                        </Form.Group>
                    </Form>
                </Container>
                {/* <button onClick={ ()=> this.props.setUser(this.state.users)}>Confirm</button> */}

            </WarpperPageTwo>
        )
    }

}

export default Page_3