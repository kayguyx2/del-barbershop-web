import React from 'react';
import { Row } from 'react-bootstrap';
import ProgressBar from '../../../containers/ProgressBar/ProgressBar';
import styled from 'styled-components';

import icon1 from '../../../assets/icon/icon1.png';
import icon2 from '../../../assets/icon/user.png';
import icon3 from '../../../assets/icon/success.png';

const Icon = styled.i`
    border-radius: 50%;
    background-color: ${props => props.color};
    color: #f5f5f5;
    padding: 5px
    z-index: 5;
    box-shadow: 5px 5px 15px 5px rgba(0,0,0,0.2);
`;

const Styled = styled.div`
    .row-icon{
        margin-top: 50px ; 
    }

`;
const LineAndIcon = styled.div`

    .row-icon2{
        position: relative;
        z-index: 1;
        margin-top :10px;
        margin-bottom :10px;
        
        
    }
    .line{
        margin-top : 8px;
        z-index: -2;
        width: 100%;
        position: absolute;
        @media (max-width: 575.98px) {
            margin-top :5px;
        }

    }
`;

const SuccessLine = props => (
    <Styled>
        <Row className="row-icon">
            <div
                className="col-4 text-center"
            ><img
                    className="img-responsive"
                    src={icon1}
                    width={60}
                    height={60}
                    alt="icon." />
            </div>
            <div className="col-4 text-center"
            ><img
                    className="img-responsive"
                    src={icon2}
                    width={60}
                    height={60}
                    alt="icon." />
            </div>
            <div className="col-4 text-center"
            ><img
                    className="img-responsive"
                    src={icon3}
                    width={60}
                    height={60}
                    alt="icon." />
            </div>
        </Row>
        <LineAndIcon>
            <Row className="row-icon2">
                <div className="line" >
                    <ProgressBar
                        percentage={props.clamp(0, props.percentage,
                            100)}
                    />
                </div>
                <div className="col-4 text-center" >
                    <Icon
                        color={props.colorIcon.icon1}
                        className="fas fa-check">
                    </Icon>
                </div>
                <div className="col-4 text-center" >
                    <Icon
                        color={props.colorIcon.icon2}
                        className="fas fa-check">
                    </Icon>
                </div>
                <div className="col-4 text-center" >
                    <Icon
                        color={props.colorIcon.icon3}
                        className="fas fa-check">
                    </Icon>
                </div>
            </Row>
        </LineAndIcon>
    </Styled>

);

export default SuccessLine;