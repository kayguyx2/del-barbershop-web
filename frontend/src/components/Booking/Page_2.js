import React, { Component } from 'react';
import Card from '../Card/MainCard';
import styled from 'styled-components';

const WarpperPageTwo = styled.div`
    h3 {
        margin-top: 25px;
        font-family: 'Acme', sans-serif;
        text-shadow: 2px 2px 15px rgba(206,51,44,0.73);
        text-align: center;
    }

    .crad-barber{
        text-align: center;
    }
`;
export default class Page_2 extends Component {
    constructor(props) {
        super(props);
        this.state ={

        }
        
    }
    
    componentDidMount() {
        this.props.callAdmin();
    }
    
    handleChangeAdmin = (id) => {
        console.log(id); 
        this.props.setAdminId(id);
    }
    render() {
        return (
            <WarpperPageTwo>
                <h3>Select Stylist</h3>
                <div className="crad-barber">
                    {this.props.Admin.map((admin, index) => 
                    {
                        const cssName = this.props.activeIndex === index ? 'MuiEngagementCard--02' : 'MuiEngagementCard--01';
                        console.log(this.props.activeIndex);
                        
                        return (
                            <Card
                                dis={true}
                                key={index}
                                index={index}
                                adminId={admin._id}
                                name={admin.name}
                                cssName={cssName}
                                img={'http://localhost:8000/' + admin.imageUrl}
                                setAdminId={this.handleChangeAdmin}
                            />
                        )
                    }
                    )}
                </div>
            </WarpperPageTwo>

        )
    }
}
