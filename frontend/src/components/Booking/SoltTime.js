import React, { Component } from 'react'
import styled from 'styled-components';


const ProgressBarContainer = styled.div`
    .media{
        text-align: center;
        font-family: 'Roboto', sans-serif;
        border: 5px double SlateGrey;
        border-radius: 5px;
        width: 90%;
        padding: 2% 0 2% 0;
        color: black;
        background-color: rgb(255,255,255);
        margin: 0 auto;
        &:hover {
            background-color: rgba(51,255,119,0.7);
            border: 5px double #f5f5f5;
            box-shadow: 2px 3px 15px -3px #000000;
            font-weight: bold;
            color: black;
        }
    }
    p {
        margin: 0 auto;
        text-align: center;
      }
    .clickmedia{
        font-family: 'Roboto', sans-serif;
        border-radius: 5px;
        width: 90%;
        text-align: center;
        padding: 2% 0 2% 0;
        color: black;
        background-color: rgba(51,255,119,0.7);
        border: 5px double #f5f5f5;;
        margin: 0 auto;
        font-weight: bold;
        box-shadow: 2px 3px 15px -3px #000000;
    }
`;



class SoltTime extends Component {

    render() {
        return (
            <ProgressBarContainer>
                <div
                    className={this.props.cssName}
                    key={this.props.keydiv}
                    onClick={() => this.props.setTime(this.props.keydiv,this.props.time)}
                >
                    <p><i className="far fa-clock"></i>{"  "}{this.props.text}</p>
                </div> 
            </ProgressBarContainer>
        )
    }
}

export default SoltTime;