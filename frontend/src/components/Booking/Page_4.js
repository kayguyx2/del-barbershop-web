import React from 'react'
import moment from 'moment';
import Card from '../Card/MainCard';
import styled from 'styled-components';

const WarpperPageTwo = styled.div`
    text-align: center;
    
    margin-top: 25px;

    .black_box {
        padding: 35px;
        width: 100%;
        height: 100%;
        background-color: rgba(192,192,192,0.21);
        float:left;
        border-radius: 10px;
        border: 2px dashed #9B8205;
        outline-offset: -10px;

        @media only screen and (max-width: 600px) {
            padding: 25px 0;
        }
    }
    h3 {
        margin-top: 25px 0;
        font-family: 'Acme', sans-serif;
        text-shadow: 2px 2px 15px rgba(206,51,44,0.73);
        text-align: center;
    }

    .input-form-p3{
        padding-left: 15px;
    }
`;
const Page_4 = props => (
    <WarpperPageTwo>
        <div className="black_box">
            <h3>Booking information</h3>
            <hr />
            <p><span style={{ fontWeight: "bold" }}>Book a date{" "}:{" "}</span>
                {moment(props.data.time).format('LLLL')}</p>
            {props.admin.filter(ad => ad._id === props.data.adminId).map((admin, index) => (
                <Card
                    dis={false}
                    key={index}
                    name={admin.name}
                    cssName="MuiEngagementCard--03"
                    img={'http://localhost:8000/' + admin.imageUrl}
                />))}
            <p>Create By : {props.data.name}</p>
            <p>Phone : {props.data.phone}</p>
            <p>Email : {props.data.email}</p>
        </div>

    </WarpperPageTwo>
);

export default Page_4;