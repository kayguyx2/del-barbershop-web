import React, { Component } from 'react'
import styled from 'styled-components';

// call class
import Solttime from '../Booking/SoltTime';
import NoTime from './PickTime/NoTime';

// call method 
import moment from 'moment';
import DatePicker from "react-datepicker";
import addDays from "date-fns/addDays";
import "react-datepicker/dist/react-datepicker.css";


const Styled = styled.div`
    .form-input {   
        margin-left: 25px;
        margin-top: 20px;
    }
    .form-input-date{   
        color: #6bccf9;
        text-align: center;
        margin-left: 25px;
        margin-bottom: 20px;
    }
    .form-input-time{
        margin-left: 25px;
    }
    .form-input-time-data{
        width: 260px;
        margin-left: 25px;
        margin-bottom: 125px;
        text-align: center;
    }
`;
const H3 = styled.h3`
        margin-top: 25px;
        font-family: 'Acme', sans-serif;
        text-shadow: 2px 2px 15px rgba(206,51,44,0.73);
        text-align: center;
        color: rgba(0,0,0,0.8);
`;
const H5 = styled.h5`
        margin-top: 25px;
        font-family: 'Acme', sans-serif;
        text-align: left;
        color: rgba(0,0,0,0.8);
`;

const divStyle = {
    marginBottom: '10px',
    textAlign: 'center'
};

class Page_1 extends Component {
    render() {

        return (
            <Styled>
                <H3>Select Date and Time</H3>
                <H5>Pick a Date</H5>
                <div className="form-input"  >
                    <DatePicker
                        className="form-input-date"
                        selected={this.props.startDate}
                        onChange={this.props.handleChange}
                        dateFormat="dd/MM/yyyy"
                        minDate={new Date()}
                        maxDate={addDays(new Date(), 5)}
                        placeholderText="--- Select ---"
                    />
                </div>
                <H5>Pick a Time</H5>
                <div className="form-input-time" >
                    <hr />
                    {
                        this.props.generateTime.map(function (time, index) {
                            const cssName = this.props.activeIndex === index ? 'clickmedia' : 'media';
                            return (
                                <div key={index} style={divStyle}>
                                    <Solttime
                                        id={index}
                                        keydiv={index}
                                        cssName={cssName}
                                        text={moment(time).format('LT')}
                                        setTime={this.props.handleChangeTime.bind(this, index,time)}
                                        time={time}
                                    />
                                </div>
                            )
                        }, this)
                    }
                    {(this.props.generateTime.length === 0) && <NoTime />}


                </div>
            </Styled>
        )
    }
}

export default Page_1;