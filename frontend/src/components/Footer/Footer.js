import React from 'react';
import styled from 'styled-components';

function Footer() {
  return (
    <FooterContainer className="main-footer">
      <div className="footer-middle">
        <div className="container">
          {/* Footer Bottom */}
          <div className="footer-bottom">
            <p className="text-xs-center">
              &copy;{new Date().getFullYear()} BarberShop App - All Rights
              Reserved
            </p>
          </div>
        </div>
      </div>
    </FooterContainer>
  );
}
export default Footer;

const FooterContainer = styled.footer`
  .footer-middle {
    background: rgba(0,0,0,0.8);
    color: #f5f5f5;
  }
  .text-xs-center{
    text-align: right;
  }
  .footer-bottom {
    padding-top: 5vh;
    padding-bottom: 3vh;
  }

  ul li a {
    color: #f5f5f5;
  }

  ul li a:hover {
    color: rgba(0,0,0,0.8);
  }
`;
