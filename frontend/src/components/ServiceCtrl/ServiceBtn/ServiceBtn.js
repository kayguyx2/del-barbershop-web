import React from 'react';
import styled from 'styled-components';

const Styles = styled.div`

    .Label {
        text-align: center;
        font-family: Roboto;
        padding: 10px
        font-weight: bold;
        margin: auto;
    }

    .Non {
        background: white;
        color: rgba(0,0,0,0.8);
        display: inline-block;
        font-size: 1em;
        margin: 0.25em;
        border: 2px solid rgba(0,0,0,0.8);
        border-radius: 8px;
        width: 100px;
        height: 50px;
      
        &:hover {
          background: tomato;
          color: white ; 
        }
        &:disabled {
            color: white;
            background: tomato;
        }
    }

    .Select {
        background: white;
        color: rgba(0,0,0,0.8); 
        display: inline-block;
        bottom: 150px
        border: 2px solid rgba(0,0,0,0.8);
        border-radius: 8px;
        width: 100px;
        height: 50px;
      
        &:hover {
          background: mediumseagreen;
          color: white ; 
        }
        &:disabled {
            color: white;
            background: mediumseagreen;
        }
        
    }
    
`;

const ServiceBtn = props => (
  <Styles>
    <div className="">
      <div className="Label">{props.label}</div>
      <button
        className="Non"
        onClick={props.switchs}
        disabled={!props.disabled}
      >
        No
      </button>
      {"  "}
      <button
        className="Select"
        onClick={props.switchs}
        disabled={props.disabled}
      >
        Select
      </button>
    </div>
  </Styles>
);

export default ServiceBtn;
