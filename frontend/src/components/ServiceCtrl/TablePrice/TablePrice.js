import React from 'react'
import styled from 'styled-components';
import img from '../../../assets/service.jpg';

const CtrlImg = styled.div`
  text-align: center;
  position: relative;

  .img {
    
    max-width: 100%
    min-width:300px
    height: auto
    display:block
    margin-top: 10px;
    display: block;
    margin-left: auto;
    margin-right: auto;
  }
`;

const FormatPrice = styled.div`
    text-align: center;
    font-family: 'Roboto', sans-serif;
    background-color: rgba(255,255,255,0.95);
    position: absolute;
    width: 100%
    z-index: 50;
    top:18%;
    padding-top: 5%;
    text-shadow: 0px 0px 20px #FFFFFF;

    @media (max-width: 992.98px ) {
        top: 0%;
        min-height: 100%;
        background-color: rgba(255,255,255,0.8);
    }
    h5{
        font-family: 'Roboto', sans-serif;
        font-weight: bold;
    }
    .span{
      font-family: 'Roboto', sans-serif;
      color : FireBrick;
      font-weight: bold;
    }
    .span-nm{
      font-family: 'Roboto', sans-serif;
      
    }
`;
const TablePrice = props => (
    <React.Fragment>
        <CtrlImg>
            <img
                className="img"
                src={img}
                width={480}
                height={360}
                alt="responsive img"
            />
            <FormatPrice>
                <h5>Hair Cut</h5>
                <p>
                    <span className="span-nm">Man</span>:
              <span className="span">{" "}{props.cutman} THB.</span>
                    <br />
                    <span className="span-nm">Student:</span>
                    <span className="span">{" "}{props.cutstudent} THB.</span>
                </p>
                <h5>Shave</h5>
                <p>
                    <span className="span-nm">Man and Student</span>:
              <span className="span">{" "}{props.shave} THB.</span>
                </p>
            </FormatPrice>
        </CtrlImg>

    </React.Fragment>

);



export default TablePrice;