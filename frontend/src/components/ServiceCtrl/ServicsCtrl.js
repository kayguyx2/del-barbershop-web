import React from 'react';
import styled from 'styled-components';
import { Container, Row, Col } from 'react-bootstrap';
import ServiceBtn from './ServiceBtn/ServiceBtn';
import ServiceAge from './ServiceAge/ServiceAge';
import TablePrice from './TablePrice/TablePrice';

const Styled = styled.div`
  text-align: center;
`;

const Button = styled.button`
  background: white;
  color: palevioletred;
  display: inline-block;
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;
  text-align: center;
  &:hover {
    background: palevioletred;
    color: white;
  }

  &:disabled {
    border: 2px solid DarkGrey;
    color: white;
    background: DarkGrey;
  }
`;
const SuccessBtn = styled(Button)`
  margin: 0 auto;
  color: mediumseagreen;
  border: 2px solid mediumseagreen;
`;


const controls = [
  { label: 'HairCut ', type: 'hairCut' },
  { label: 'Shave ', type: 'shave' },
];

const ServerCtrl = props => (
  <Styled>
    <Container>
      <Row>
        <Col>
          <TablePrice
            cutman={props.cutman}
            cutstudent={props.cutstudent}
            shave={props.shave}
          />
        </Col>
        <Col className="col-cen">
          <ServiceAge
            key="Choose"
            label="Choose "
            chooseMan={() => props.chooseManServices()}
            chooseYoung={() => props.chooseYoungServices()}
            disableM={props.disabledChooseMan}
            disableY={props.disabledChooseYoung}
          />
          {controls.map(ctrl => (
            <ServiceBtn
              key={ctrl.label}
              label={ctrl.label}
              switchs={() => props.switchServices(ctrl.type)}
              disabled={props.disabled[ctrl.type]}
            />
          ))}
        </Col>
      </Row>

      <br />
      <hr />
      <h5 className="col-cen">
        Current Price : {props.price.toFixed(2)} BTH
    </h5>
      <SuccessBtn disabled={!props.disabledPayment} onClick={props.checkbill}>
        Check bill
    </SuccessBtn>
      <br />
    </Container>
  </Styled>
);

export default ServerCtrl;
