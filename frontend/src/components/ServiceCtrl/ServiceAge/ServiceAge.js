import React from 'react';
import styled from 'styled-components';

// rgba(0,0,0,0.8);
const Styles = styled.div`

    .Label {
        font-family: Roboto;
        font-weight: bold;
        margin: 5px auto;
    }

    .Non {
        background: white;
        color: rgba(0,0,0,0.8); 
        display: inline-block;
        font-size: 1em;
        margin: 0.25em;
        
        border: 2px solid rgba(0,0,0,0.8);
        border-radius: 8px;
        width: 100px;
        height: 50px;
        outline: none !important;
        &:hover {
          background: tomato;
          color: white ; 
          
        }
        &:disabled {
            color: white;
            background: tomato;
        }
    }

    .Select {
        background: white;
        color: rgba(0,0,0,0.8);
        display: inline-block;
        bottom: 150px
        border: 2px solid rgba(0,0,0,0.8);
        border-radius: 8px;
        width: 100px;
        height: 50px;
        outline: none !important;
        &:hover {
          background: mediumseagreen;
          color: white ; 
        }
        &:disabled {
            color: white;
            background: mediumseagreen;
        }
        
    }


    
`;

const ServiceBtn = props => (
  <Styles>
    <div className="">
      <div className="Label">{props.label}</div>
      <button
        className="Non"
        onClick={props.chooseYoung}
        disabled={props.disableY}
      >Student
      </button>
      {"  "}
      <button
        className="Select"
        onClick={props.chooseMan}
        disabled={props.disableM}
      >
        Man
      </button>
    </div>
  </Styles>
);

export default ServiceBtn;
