import React, { Fragment } from 'react';

import Modal from '../Modal/Modal';


const errorHandler = props => (
  <Fragment>
    {/* {props.error && <Backdrop onClick={props.onHandle} />} */}
    {props.error && (
        <Modal
          // title="An Error Occurred"
          // onCancelModal={props.onHandle}
          // onAcceptModal={props.onHandle}
          // acceptEnabled
        >
          <h1>this error</h1>
          <p>{props.error.message}</p>
          <button onClick={props.onHandle}>click</button>
        </Modal>
    )}
  </Fragment>
);

export default errorHandler;
