import React, { Component } from 'react'
import Chart from "react-apexcharts";
import moment from 'moment';
import styled from 'styled-components';
// call css 
import Button from 'react-bootstrap/Button';
const Styled = styled.div`
    margin: 15px;
    .btn-right{
        text-align: right;
    }
`;
class ChartsService extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                chart: {
                    id: 'mychart',
                    background: '#f4f4f4',
                    foreColor: '#333',
                },
                xaxis: {
                    categories: [],
                    title: {
                        text: `Date: ${moment().startOf('month').format('DD-MM-YYYY')} - ${moment().format('DD-MM-YYYY')}`,
                    }
                },
                title: {
                    text: 'Number Serviecs',
                    align: 'center',
                    margin: 20,
                    offsetY: 20,
                    style: {
                        fontSize: '20px'
                    }
                },
                dataLabels: {
                    enabled: false
                },
                plotOptions:{
                    bar: {
                        horizontal: true
                    }
                },
                fill: {
                    colors: ['#CD5C5C'],
                    opacity: 0.9,
                }

            },
            series: [
                {
                    name: 'Service',
                    data: [30, 40],
                }
            ]
        };
    }
    componentDidMount() {
        this.callAdmin();
    }

    // call all fetchService 
    callAdmin = () => {
        this.setState({ isLoading: true });
        const requestData = {
            query: `
                query{
                    callAdmin{
                      name
                      imageUrl
                      _id
                      createdService{
                          _id
                      }
                    }
                }
            `
        };
        fetch('http://localhost:8000/graphql', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestData),
        })
            .then(res => {
                return res.json();
            })
            .then(resData => {

                const barber = resData.data.callAdmin.map(m => m.name);
                const event = resData.data.callAdmin.map(m => m.createdService.length)
                this.setState({
                    options: {
                        ...this.state.options,
                        xaxis: {
                            ...this.state.options.xaxis,
                            categories: barber
                        }
                    }
                });

                const newSeries = [];
                newSeries.push({ data: event, name: "Booking" })
                console.log(newSeries);
                this.setState({
                    series: newSeries
                })

            })
            .catch(err => {
                this.setState({ isLoading: false });
                console.log(err);
            });
    }
    onHorizontal = () =>{
        this.setState({
            options:{
                ...this.state.options,
                plotOptions:{
                    ...this.state.options.plotOptions,
                    bar:{
                        ...this.state.options.plotOptions.bar,
                        horizontal: !this.state.options.plotOptions.bar.horizontal
                    }
                }
            }
        })
    }
    render() {
        return (
            <Styled>
                <Chart
                    options={this.state.options}
                    series={this.state.series}
                    type="bar"
                    height="300"
                    width="100%"
                />
                <div className="btn-right">
                    <Button  variant="info" onClick={this.onHorizontal}><i className="fas fa-undo-alt"></i></Button>
                </div>
                
            </Styled>

        )
    }
}

export default ChartsService;