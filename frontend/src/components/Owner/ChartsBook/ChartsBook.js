import React, { Component } from 'react'
import Chart from "react-apexcharts";
import moment from 'moment';
// call css 
import Button from 'react-bootstrap/Button';
import styled from 'styled-components';
const Styled = styled.div`
    margin: 15px;
    .btn-right{
        text-align: right;
    }
`;
class ChartsBook extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                chart: {
                    id: 'mychart',
                    background: '#f4f4f4',
                    foreColor: '#333',
                },
                xaxis: {
                    categories: [],
                    title: {
                        text: `Date: ${moment().startOf('month').format('DD-MM-YYYY')} - ${moment().format('DD-MM-YYYY')}`,
                    }
                },
                title:{
                    text: 'Number Booking',
                    align: 'center',
                    margin: 20,
                    offsetY: 20,
                    style: {
                        fontSize: '20px'
                    }
                },
                dataLabels:{
                    enabled: false
                },
                fill:{
                    colors: ['#4682B4'],
                    opacity: 0.9,
                },
                plotOptions:{
                    bar: {
                        horizontal: false
                    }
                },

            },
            series: [
                {
                    name: 'Booking',
                    data: [20,30],
                }
            ]
        };
    }
    componentDidMount() {
        this.callAdmin();
    }
    onHorizontal = () =>{
        this.setState({
            options:{
                ...this.state.options,
                plotOptions:{
                    ...this.state.options.plotOptions,
                    bar:{
                        ...this.state.options.plotOptions.bar,
                        horizontal: !this.state.options.plotOptions.bar.horizontal
                    }
                }
            }
        })
    }
    // call all barber 

    callAdmin = () => {
        this.setState({ isLoading: true });
        const requestData = {
            query: `
                query{
                    callAdmin{
                      name
                      imageUrl
                      _id
                      createdEvents{
                          _id
                      }
                    }
                }
            `
        };
        fetch('http://localhost:8000/graphql', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestData),
        })
            .then(res => {
                return res.json();
            })
            .then(resData => {

                const barber = resData.data.callAdmin.map(m => m.name);
                const event = resData.data.callAdmin.map(m => m.createdEvents.length)
                this.setState({
                    options: {
                        ...this.state.options,
                        xaxis: {
                            ...this.state.options.xaxis,
                            categories: barber
                        }
                    }
                });

                const newSeries = [];
                newSeries.push({ data: event, name: "Booking" })
                console.log(newSeries);
                this.setState({
                    series: newSeries
                })

            })
            .catch(err => {
                this.setState({ isLoading: false });
                console.log(err);
            });
    }
    render() {
        return (
            <Styled>
                
                <Chart
                    options={this.state.options}
                    series={this.state.series}
                    type="bar"
                    height="300"
                    width="100%"
                />
                <div className="btn-right">
                    <Button  variant="info" onClick={this.onHorizontal}><i className="fas fa-undo-alt"></i></Button>
                </div>
            </Styled>

        )
    }
}

export default ChartsBook;