import React from 'react';
import ReactModal from 'react-modal';
import { Form, Col, Row, Container } from 'react-bootstrap';
import { Input } from 'reactstrap';
import './EditPrice.css';

ReactModal.setAppElement(document.getElementById('root'));

class EditPrice extends React.Component {

    constructor(props) {
        super(props);
        this.cutmanEL = React.createRef();
        this.cutstudentEL = React.createRef();
        this.shaveEL = React.createRef();
    }

    render() {
        return (
            <ReactModal
                isOpen={true}
                className="Modal-Edit"
                overlayClassName="Overlay"
            >
                <div className="title-modal-edit" >
                    <h2><i className="far fa-address-book"></i> Edit Price of Service</h2>
                </div>
                <Container>
                    <hr />
                    <div className="detail-modal-edit">
                        <Form
                            onSubmit={e =>
                                this.props.accept(e, {
                                    cutman: this.cutmanEL.value,
                                    cutstudent: this.cutstudentEL.value,
                                    shave: this.shaveEL.value
                                })
                            }
                        >
                            <Form.Group as={Row}>
                                <Form.Label column sm="12">
                                    <i className="fas fa-cut"></i> Hair Cut - Man: {this.props.cutman} THB. | Student: {this.props.cutstudent} THB. Shave
                        </Form.Label>
                                <Form.Label column sm="12">
                                    Shave - Man and Student: {this.props.shave} THB.
                        </Form.Label>
                            </Form.Group>
                            <Form.Group as={Row} >
                                <Form.Label column sm="4"  style={{textAlign : "right"}}>
                                    New Man Price
                                </Form.Label>
                                <Col sm="6" >
                                    <Input
                                        type="text"
                                        placeholder="New Student Hair Cut"
                                        innerRef={(cutman) => { this.cutmanEL = cutman }}
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} >
                                <Form.Label column sm="4"  style={{textAlign : "right"}}>
                                    New Student Price
                                </Form.Label>
                                <Col sm="6">
                                    <Input
                                        type="text"
                                        placeholder="New Man Hair Cut"
                                        innerRef={(cutstudent) => { this.cutstudentEL = cutstudent }}
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} >
                                <Form.Label column sm="4"  style={{textAlign : "right"}}>
                                    New Shave
                                </Form.Label>
                                <Col sm="6">
                                    <Input
                                        type="text"
                                        placeholder="New Shave"
                                        innerRef={(shave) => { this.shaveEL = shave }}
                                    />
                                </Col>
                            </Form.Group>
                            <Row>
                                <Col className="config-bnt-edit-left">
                                    <h5>
                                        <button className="btn btn-danger" onClick={this.props.hide}>Close</button>
                                    </h5>
                                </Col>
                                <Col className="config-bnt-edit-right">
                                    <h5>
                                        <button
                                            className="btn btn-success"
                                            type="submit" >Accept
                                        </button>
                                    </h5>
                                </Col>
                            </Row>
                        </Form>
                        <hr />
                    </div>


                </Container>
            </ReactModal>
        );
    }
}
export default EditPrice;

