import React from 'react';
import ReactModal from 'react-modal';
import { Container, Button } from 'react-bootstrap';
import moment from 'moment';
import './ShowDetail.css';

ReactModal.setAppElement(document.getElementById('root'));

class ShowDetail extends React.Component {

    constructor(props) {
        super(props);
        this.cutmanEL = React.createRef();
        this.cutstudentEL = React.createRef();
        this.shaveEL = React.createRef();
    }

    render() {
        return (
            <ReactModal
                isOpen={true}
                className="Modal-ShowDetail"
                overlayClassName="Overlay"
            >
                <div className="title-modal-ShowDetail" >
                    <h2>Booking Detail</h2>
                </div>
                <Container>

                    <div className="detail-modal-ShowDetail">
                        <hr />
                        <p>You have booked on{" "}
                            <span
                                style={{ fontWeight: "bold" }}>{moment(this.props.time).format('lll')}
                            </span>
                        </p>
                        <p>{this.props.name + " "}has reserved time</p>
                        <p>{this.props.barber + " "}is your barber</p>
                        <p>If you have a problem You can contact : {this.props.phone + " "}</p>
                        <hr />
                    </div>
                    <div className="config-bnt-ShowDetail-right">
                        <Button
                            variant="success"
                            onClick={() => this.props.close()}>Accept</Button>
                    </div>

                </Container>
            </ReactModal>
        );
    }
}
export default ShowDetail;

