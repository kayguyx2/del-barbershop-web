import React from 'react';
import ReactModal from 'react-modal';
import './SuccessModal.css';

ReactModal.setAppElement(document.getElementById('root'));

const SuccessModal = props => (
    <ReactModal
        isOpen={true}
        className="Modal-Success"
        overlayClassName="Overlay-Success"
    >
        <div className="success-modal">
            <img
                className="success-icon"
                src="http://localhost:8000/images/model/checked.png"
                alt="icon logo"
            />
        </div>
        <div className="success-haeder-model">
            <h2>Completed</h2>
        </div>
        <div className="success-detail-modal">
            <p>{props.message}</p>
        </div>
        <div className="success-footter-model">
            <hr className="green" />
            <h3>
                <button
                    className="btn btn-success"
                    onClick={props.close}>Close
                </button>
            </h3>
        </div>

    </ReactModal>
);




export default SuccessModal;
