import React from 'react';
import ReactModal from 'react-modal';
import './ErrorModal.css';

ReactModal.setAppElement(document.getElementById('root'));

const ErrorModle = props => (
    <ReactModal
        isOpen={true}
        className="Modal"
        overlayClassName="Overlay"
    >
        <div className="error-modal">
            <img
                className="error-icon"
                src="http://localhost:8000/images/model/warning.png"
                alt="icon logo"
            />
        </div>
        <div className="error-haeder-model">
            <h2>ERROR!</h2>
        </div>
        <div className="detail-modal">
            <p>{props.message}</p>
        </div>
        <div className="error-footter-model">
            <hr className="red" />
            <h3>
                <button
                    className="btn btn-danger"
                    onClick={props.close}>Close
                </button>
            </h3>
        </div>
    </ReactModal>
);



export default ErrorModle;
