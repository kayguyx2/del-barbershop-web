import React from 'react';
import ReactModal from 'react-modal';
import { Container, Button } from 'react-bootstrap';
import moment from 'moment';
import './SentKey.css';

ReactModal.setAppElement(document.getElementById('root'));

class SentKey extends React.Component {

    constructor(props) {
        super(props);
        this.cutmanEL = React.createRef();
        this.cutstudentEL = React.createRef();
        this.shaveEL = React.createRef();
    }

    render() {
        return (
            <ReactModal
                isOpen={true}
                className="Modal-SentKey"
                overlayClassName="Overlay"
            >
                <div className="title-modal-SentKey" >
                    <h2>Succes Booking</h2>
                </div>
                <Container>
                    <hr />
                    <div className="detail-modal-SentKey">
                        <p>{this.props.name + " "}is success to Booking</p>
                        <p>You have booked on {moment(this.props.time).format('lll')}</p>
                        <div className="detail-model-Key">
                            <h3 className="key-h3">KEY :</h3> <h3 className="key-h3-2">{this.props.sentKey}</h3>
                        </div>
                        <p>This key can be edit booking information and cancel booking</p>
                        <hr />
                        <div className="config-bnt-SentKey-right">
                            <Button 
                                variant="success" 
                                onClick={() => this.props.close()}>Accept
                            </Button>
                        </div>
                    </div>


                </Container>
            </ReactModal>
        );
    }
}
export default SentKey;

