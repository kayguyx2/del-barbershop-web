import React from 'react';
import { Nav, Button } from 'react-bootstrap';
import styled from 'styled-components';
const Styles = styled.div`
  
  a,
  .navbar-nav .nav-link {
    color: rgba(0,0,0,0.8);
    text-decoration: none;
    &:hover {
      color: #bbb;
    }
  }
  .justify-content-center{
    padding: 20px;
  }
  .nav-i{
    margin-right: 10px;
  }
  .btn-c{
    color: rgba(0,0,0,0.8);
    text-decoration: none;
    background-color: rgba(0,0,0,0.05);
    border: 0px;
    display: inline-block;
    &:hover {
      background-color: rgba(0,0,0,0.8);
      color: #f5f5f5;
      2px 2px 9px 5px rgba(0,0,0,0.25)
    }
    &:focus, textarea { 
      outline: none !important;
    }
  }
  .btn-a{
    text-decoration: none;
    border: 0px;
    display: inline-block;      
    background-color: rgba(0,0,0,0.8);
    color: #f5f5f5;
    &:focus, textarea { 
      outline: none !important;
    }
  }
`;

const NavOwner = props => (

  <Styles>
    <React.Fragment>
      <Nav className="justify-content-center">
        <Nav.Item className="nav-i">
          <Button className={props.dashbordStlye} onClick={props.clickDashbord}>
            Dashbord
          </Button>
        </Nav.Item>
        <Nav.Item className="nav-i">
          <Button className={props.summaryStlye} onClick={props.clickSummary}>
          The Revenue Summary
          </Button>
        </Nav.Item>
        <Nav.Item className="nav-i">
          <Button className={props.memberStlye} onClick={props.clicklistMember}>
            List Member
          </Button>
        </Nav.Item>
      </Nav>
    </React.Fragment>
  </Styles>
);

export default NavOwner;

