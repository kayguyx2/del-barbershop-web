import React from 'react';
import { Input } from 'reactstrap';

import styled from 'styled-components';

const Styles = styled.div`
  .touched.invalid {
    border-color: red;
    background: #ffc2c2;
  }
`;
const input = props => (
  <Styles>
    {/* {props.label && <label htmlFor={props.id}>{props.label}</label>} */}
    {props.control === 'input' && (
      <Input
        className={[
          !props.valid ? 'invalid' : 'valid',
          props.touched ? 'touched' : 'untouched',
        ].join(' ')}
        id={props.id}
        type={props.type}
        value={props.value}
        placeholder={props.placeholder}
        // required={props.required}
        // onChange={e => props.onChange(props.id, e.target.value, e.target.files)}
        // onBlur={props.onBlur}
      />
    )}
    {props.control === 'textarea' && (
      <textarea
        className={[
          !props.valid ? 'invalid' : 'valid',
          props.touched ? 'touched' : 'untouched',
        ].join(' ')}
        id={props.id}
        rows={props.rows}
        required={props.required}
        value={props.value}
        onChange={e => props.onChange(props.id, e.target.value)}
        onBlur={props.onBlur}
      />
    )}
  </Styles>
);

export default input;
