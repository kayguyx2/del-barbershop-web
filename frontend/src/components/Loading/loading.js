import React from 'react';
import './loading.css';
const loading = () => (
    <React.Fragment>
        <div className="loading">Loading&#8230;</div>
    </React.Fragment>
);

export default loading;