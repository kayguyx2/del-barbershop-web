import React from 'react';
import { Nav, Button } from 'react-bootstrap';
import styled from 'styled-components';
import './ad.css';
const Styles = styled.div`
  a,
  .navbar-nav .nav-link {
    
    color: #000000;
    text-decoration: none;
    
    &:hover {
      color: #bbb;
    }
  }

  .active {
    background-color: #4caf50;
  }
  .navbar-toggler {
    background: #f0ffff;
  }

  .justify-content-center{
    padding: 20px;
  }
  .nav-i{
    margin-right: 10px;
  }
  .btn-c{
    color: rgba(0,0,0,0.8);
    text-decoration: none;
    background-color: rgba(0,0,0,0.05);
    border: 0px;
    display: inline-block;
    &:hover {
      background-color: rgba(0,0,0,0.8);
      color: #f5f5f5;
      2px 2px 9px 5px rgba(0,0,0,0.25)
    }
    &:focus, textarea { 
      background-color: rgba(0,0,0,0.8);
      color: #f5f5f5;
      text-decoration: none;
      outline: none !important;
    }
  }
  .btn-s{
    background-color: rgba(0,0,0,0.8);
    color: #f5f5f5;
    text-decoration: none;
    border: 0px;
    display: inline-block;
  }
`;

const AdminNavigation = props => (

  <Styles>
    <React.Fragment>
      <Nav className="justify-content-center">
        <Nav.Item className="nav-i">
          <Button className="btn-c"  autoFocus onClick={props.clickService}>
            Service
          </Button>
        </Nav.Item>
        <Nav.Item className="nav-i">
          <Button className="btn-c" onClick={props.clicklistservice}>
            List Service
          </Button>
        </Nav.Item>
        <Nav.Item className="nav-i">
          <Button className="btn-c"onClick={props.clicklistbook}>
            List Booking</Button>
        </Nav.Item>
        <Nav.Item className="nav-i">
          <Button className="btn-c" onClick={props.clicklistmember}>
            List Member
          </Button>
        </Nav.Item>

      </Nav>
    </React.Fragment>
  </Styles>
);

export default AdminNavigation;
