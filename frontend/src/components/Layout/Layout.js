import React from 'react';
import './Layout.css';
import styled from 'styled-components';
import { Container } from 'react-bootstrap';

const Styles = styled.div`
  .main {
    box-shadow: 0px 0px 19px 6px rgba(0,0,0,0.26);
    background-color: White; 
    min-height: 80vh;
    overflow: auto;
  }
`;

export const Layout = props => (
    <React.Fragment>
        <Styles>
            <div className="main">
                <Container>
                    {props.children} 
                </Container>
            </div>
        </Styles>
    </React.Fragment>

);