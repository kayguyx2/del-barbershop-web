import React, { Component } from 'react';
import Auxilary from '../../hoc/Auxilary';
import ServerCtrl from '../../components/ServiceCtrl/ServicsCtrl';
import Line from '../../components/HomePage/LineSubTitie/LineSubtitile';
import EditModel from '../../components/Modal/EditPrice/EditPrice';
import styled from 'styled-components';
import Load from '../../components/Loading/loading';
const SERVICE_PRICES = {
  hairCut: 0,
  hairCutYung: 150,
  hairCutMan: 250,
  shave: 50,
};

const FormatBntSetting = styled.div`
    text-align: right;
    margin-right: 5%;
    
    .btn-setting{
      background-color: LimeGreen;
      color: #f5f5f5;
      padding: 5px;
      border: 0px;
      border-radius: 10px;
      &:hover{
        opacity: 0.8;
      }
      &:focus{
        outline: 0;
      }
    }
`;

class ServiceBuilder extends Component {
  constructor(props){
    super(props)
    this.state = {
      service: {
        hairCut: false,
        shave: false,
      },
      adulthood: {
        young: false,
        man: false,
      },
      totalPrice: 0,
      purchasable: false,
      EditPrice: false,
      loader: false,
      service_prices:{
        cutman: null,
        cutstudent: null,
        shave: null
      }
    };

    this.showEditPrice = this.showEditPrice.bind(this);
    this.hideEditPrice = this.hideEditPrice.bind(this);
  }

  componentDidMount() {
    this.callPrice();
    this.updatePurchaseState(this.state.service);
    
  }

    // ------------------- Edit Price ---------------------
    updatePriceService = (event, inputPrice) => {
      event.preventDefault();
      this.setState({ loader: true });
      const graphqlQuery = {
        query: `
          mutation UpdatePriceService($cutman: Int!, $cutstudent: Int!, $shave: Int!){
            updatePriceService(priceInput: {cutman: $cutman, cutstudent: $cutstudent, shave: $shave}){
              cutman
              cutstudent
              shave
              updatedAt
            }
          }
        `,
        variables: {
          cutman: Number(inputPrice.cutman),
          cutstudent: Number(inputPrice.cutstudent),
          shave: Number(inputPrice.shave)
        },
      };
      fetch('http://localhost:8000/graphql', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(graphqlQuery),
      })
        .then(res => res.json())
        .then(resData => {
          const cutman = Number(resData.data.updatePriceService.cutman);
          const cutstudent = Number(resData.data.updatePriceService.cutstudent);
          const shave = Number(resData.data.updatePriceService.shave);
          
          this.setState({
            EditPrice: false,
            loader: false,
            service_prices:{
              cutman: cutman,
              cutstudent: cutstudent,
              shave: shave
            }
          });
          this.callPrice();
          console.log("End");
          
          
        })
        .catch(err => {
          console.log("End Game :",err);
          this.setState({
            EditPrice: false,
            loader: false,
            errors: err,
          });
        });
    };

    // ------------------- call Price ---------------------
    callPrice = () => {
      
      const graphqlQuery = {
        query: `
          query{
            callPriceServic{
              cutman
              cutstudent
              shave
              updatedAt
            }
          }
        `
      }
      this.setState({ loader: true });
      fetch('http://localhost:8000/graphql', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(graphqlQuery),
      })
      .then(res => res.json())
      .then(resData => {
        
        this.setState({
          loader: false,
          service_prices:{
            cutman: resData.data.callPriceServic.cutman,
            cutstudent: resData.data.callPriceServic.cutstudent,
            shave:  resData.data.callPriceServic.shave
          }
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          loader: false,
          errors: err
        });
      });
    }
  
  showEditPrice(){
    this.setState({ EditPrice: true });
  }

  hideEditPrice(){
      this.setState({ EditPrice: false})
  }
  checkbillHandler = event => {
    event.preventDefault();
    let generation = 'yung';
    if (this.state.adulthood.man) {
      generation = 'men';
    }
    this.setState({
      loader: true
    })
    const graphqlQuery = {
      query: `
              mutation CreateNewService($generation: String!, $haircut: Boolean!, $shave: Boolean!, $totalprice: Int! ) {
                createServices(servicesInput: {generation: $generation,haircut: $haircut,shave: $shave, totalprice: $totalprice}) {
                    totalprice
                    creator{
                        name
                    }
                }
              }
            `,
      variables: {
        generation,
        haircut: this.state.service.hairCut,
        shave: this.state.service.shave,
        totalprice: this.state.totalPrice,
      },
    };
    fetch('http://localhost:8000/graphql', {
      method: 'POST',
      body: JSON.stringify(graphqlQuery),
      headers: {
        Authorization: `Bearer ${this.props.token}`,
        'Content-Type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(resData => {
        this.setState({
          service: {
            hairCut: false,
            shave: false,
          },
          adulthood: {
            young: false,
            man: false,
          },
          totalPrice: 0,
          purchasable: false,
          loader: false
        });
        //   this.props.history.replace('/');
      })
      .catch(err => {
        console.log(err);
        this.setState({
          service: {
            hairCut: false,
            shave: false,
          },
          adulthood: {
            young: false,
            man: false,
          },
          totalPrice: 0,
          purchasable: false,
          loader: false
        });
      });
  };

  updatePurchaseState(service) {
    const ser = Object.keys(service).map(igKey => service[igKey]);

    this.setState({
      purchasable:
        (ser[0] || ser[1]) &&
        (this.state.adulthood.man || this.state.adulthood.young),
    });
  }

  updatePurchaseStateChoose(service) {
    const ser = Object.keys(service).map(igKey => service[igKey]);

    this.setState({
      purchasable:
        (ser[0] || ser[1]) &&
        (this.state.service.hairCut || this.state.service.shave),
    });
  }

  chooseManServicesHandler = () => {

    // ดูราคาเก่า 
    const oldCount = this.state.adulthood.man;
    // สร้างตัวเช๊คราคา
    let updatedCount;
    if (!oldCount) {  
      updatedCount = true;
    } else {
      updatedCount = false;
    }

    // copy state adulthood
    const updatedService = {
      ...this.state.adulthood,
    };


    updatedService.man = updatedCount;
    updatedService.young = false;

    const selectAge = this.state.adulthood.man;
    const selectAge2 = this.state.adulthood.young;

    const selectService = this.state.service.hairCut;

    let priceAddition;
    
    if (selectService === true && (!selectAge && !selectAge2)) {
      priceAddition = SERVICE_PRICES.hairCutMan;
      console.log("priceAddition :", priceAddition); // 200
    } else if (selectService && selectAge2) {
      priceAddition = SERVICE_PRICES.hairCutMan - SERVICE_PRICES.hairCutYung;
    } else {
      priceAddition = SERVICE_PRICES.hairCut;
    }


    
    
    const oldPrice = this.state.totalPrice; // 0
    let newPrice = 0; // 0 

    // first time
    if (!oldCount && (!selectAge && !selectAge2)){
      newPrice = oldPrice + priceAddition;
    }

    // not first time
    if(newPrice === 0){
      if (!oldCount && (!selectAge && selectAge2)) {
        newPrice = oldPrice + priceAddition;
      } else {
        newPrice = oldPrice - priceAddition;
      }
    }


    this.setState({
      totalPrice: newPrice,
      adulthood: updatedService,
    });
    this.updatePurchaseStateChoose(updatedService);
  };

  chooseYoungServicesHandler = () => {
    const oldCount = this.state.adulthood.yong;
    let updatedCount;
    if (!oldCount) {
      updatedCount = true;
    } else {
      updatedCount = false;
    }

    const updatedService = {
      ...this.state.adulthood,
    };

    updatedService.young = updatedCount;
    updatedService.man = false;

    const selectAge = this.state.adulthood.man;
    const selectAge2 = this.state.adulthood.young;

    const selectService = this.state.service.hairCut; // ture or flase

    let priceAddition;
      // f && t
    if (selectService === true && (!selectAge && !selectAge2)) {
      priceAddition = SERVICE_PRICES.hairCutYung;
    } else if (selectService && selectAge) {
      priceAddition = SERVICE_PRICES.hairCutMan - SERVICE_PRICES.hairCutYung;
      // console.log(priceAddition);
    } else {
      priceAddition = SERVICE_PRICES.hairCut;
    }
    const oldPrice = this.state.totalPrice;
    let newPrice;
    if (!oldCount && (!selectAge && !selectAge2)) {
      newPrice = oldPrice + priceAddition;
    } else {
      newPrice = oldPrice - priceAddition;
    }

    this.setState({
      totalPrice: newPrice,
      adulthood: updatedService,
    });
    this.updatePurchaseStateChoose(updatedService);
  };

  switchHandler = type => {

    const oldCount = this.state.service[type];
    let updatedCount;
    if (!oldCount) {
      updatedCount = true;
    } else {
      updatedCount = false;
    }

    const updatedService = {
      ...this.state.service,
    };
    updatedService[type] = updatedCount;
    
    const selectAge = this.state.adulthood.man;
    const selectAge2 = this.state.adulthood.young;
    
    if (type === 'hairCut' && selectAge === true) {
      type = 'hairCutMan';
    } else if (type === 'hairCut' && selectAge2 === true) {
      type = 'hairCutYung';
    }
    const priceAddition = SERVICE_PRICES[type];
    const oldPrice = this.state.totalPrice;
    let newPrice;
    if (!oldCount) {
      newPrice = oldPrice + priceAddition;
    } else {
      newPrice = oldPrice - priceAddition;
    }

    this.setState({
      totalPrice: newPrice,
      service: updatedService,
    });
    this.updatePurchaseState(updatedService);
  };

  render() {

    // Set SERVICE_PRICES == State 
    SERVICE_PRICES.hairCutYung = this.state.service_prices.cutstudent
    SERVICE_PRICES.hairCutMan = this.state.service_prices.cutman
    SERVICE_PRICES.shave = this.state.service_prices.shave


    const disabledMan = this.state.adulthood.man;
    const disabledYoung = this.state.adulthood.young;

    const disabledInfo = {
      ...this.state.service,
    };

    return (
      <Auxilary>
        { this.state.EditPrice && 
        (<EditModel
          cutman={this.state.service_prices.cutman}
          cutstudent={this.state.service_prices.cutstudent}
          shave={this.state.service_prices.shave}
          hide={this.hideEditPrice}
          accept={this.updatePriceService}
        />)        
        }
        {this.state.loader && <Load />}
        <Line subTitle="Service Page" stylez="style-eight" />
        <FormatBntSetting>
          <button className="btn-setting" onClick={this.showEditPrice}><i className="fas fa-cog"></i>{" "}setting</button>
        </FormatBntSetting>
        <br />
        <ServerCtrl
          disabled={disabledInfo}
          disabledChooseMan={disabledMan}
          disabledChooseYoung={disabledYoung}
          chooseYoungServices={this.chooseYoungServicesHandler}
          chooseManServices={this.chooseManServicesHandler}
          switchServices={this.switchHandler}
          price={this.state.totalPrice}
          disabledPayment={this.state.purchasable}
          onClick={this.state}
          checkbill={this.checkbillHandler}
          cutman={this.state.service_prices.cutman}
          cutstudent={this.state.service_prices.cutstudent}
          shave={this.state.service_prices.shave}
        />
      </Auxilary>
    );
  }
}

export default ServiceBuilder;
