import React, { Component } from 'react'
import styled from 'styled-components';
import PropTypes from 'prop-types';


const Track = styled.div`
    width: 100%;
    height: 15px
    background-color: SlateGrey;
    border-radius: 10px;
    box-shodow: inset 0 0 5px #000;

`;

const Tumb = styled.div`
    width: ${props =>props.percentage}%;
    height: 100%;
    background-color: #6bccf9;
    border-radius: 8px;
    transition: width 0.3s ease-in-out;
`;


export default class ProgressBar extends Component {


    render() {
        return (
            <Track>
                <Tumb percentage={this.props.percentage} />
            </Track>
        )
    }
}

ProgressBar.propTypes  = {
    percentage: PropTypes.number,
};