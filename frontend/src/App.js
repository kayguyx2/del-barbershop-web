/* eslint-disable no-self-compare */

import React, { Component, Fragment } from 'react';

import { Route, Switch, Redirect, withRouter } from 'react-router-dom';


import HomePage from './pages/HomePage';
import Handle from './pages/Handle';
import Contact from './pages/Contact';
import SingUp from './pages/SectionAdmin/SignUp';
import BookingPage from './pages/BookingPage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import AdminPage from './pages/AdminPage';
import MainNavigation from './components/MainNavigation/MainNavigation';
import Banner from './components/HomePage/Banner/BannerNavBar';
import Footer from '../src/components/Footer/Footer';
import { Layout } from './components/Layout/Layout';
import Loading from './components/Loading/loading';
import MainOwner from './pages/SectionOwner/Main';
import HandleAd from './pages/SectionOwner/HandleAdmin';
import ProfileAdmin from './pages/SectionAdmin/ProfileAdmin';
import ProfileUser from './pages/ProfileUser';
import Band from './pages/Band';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showBackdrop: false,
      showMobileNav: false,
      isAuth: false,
      token: null,
      userId: null,
      authLoading: false,
      permissionCode: null,
      successSigup: false,
      errors: null
    };
  }




  componentWillMount() {
    const token = localStorage.getItem('token');
    const expiryDate = localStorage.getItem('expiryDate');
    const permissionCode = parseInt(localStorage.getItem('permissionCode'));
    if (!token || !expiryDate) {
      return console.log("don't have token");
    }
    if (new Date(expiryDate) <= new Date()) {
      console.log('have more time');
      this.logoutHandler();
      return;
    }
    const userId = localStorage.getItem('userId');
    const remainingMilliseconds =
      new Date(expiryDate).getTime() - new Date().getTime();
    this.setState({
      isAuth: true,
      token,
      userId,
      permissionCode,
    });
    this.setAutoLogout(remainingMilliseconds);
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  logoutHandler = () => {
    this.setState({
      isAuth: false,
      token: null,
      permissionCode: null,
      userId: null
    });
    localStorage.removeItem('token');
    localStorage.removeItem('expiryDate');
    localStorage.removeItem('userId');
    localStorage.removeItem('permissionCode');
  };

  setAutoLogout = milliseconds => {
    setTimeout(() => {
      this.logoutHandler();
    }, milliseconds);
  };

  setAutoBandLogout = () =>{
    setTimeout(() => {
      this.logoutHandler();
    }, 10000);
  }
  errorHandler = () => {
    this.setState({ errors: null });
    console.log('errorHandler');
  };

  successHandler = () => {
    this.setState({
      key: '',
      successSigup: false
    });
    console.log('successSigup');
  };

  loginHandler = (event, authData) => {
    event.preventDefault();
    const graphqlQuery = {
      query: `
        query UserLogin($email: String!, $password: String!) {
          login(email: $email, password: $password) {
            token
            userId
            permissionCode
          }
        }
      `,
      variables: {
        email: authData.email,
        password: authData.password,
      },
    };
    this.setState({ authLoading: true });
    fetch('http://localhost:8000/graphql', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(graphqlQuery),
    })
      .then(res => res.json())
      .then(resData => {
        if (resData.errors && resData.errors[0].status === 401) {
          throw new Error(
            "User or Passworf wrong"
          );
        }
        if (resData.errors && resData.errors[0].status === 402) {
          throw new Error(
            "User or Passworf wrong"
          );
        }
        if (resData.errors && resData.errors[0].status === 403) {
          throw new Error(
            "isEmpty Data"
          );
        }
        if (resData.errors) {
          throw new Error('User login failed!');
        }
        if (!resData.errors) {
          console.log("no error");
          this.setState({
            isAuth: true,
            token: resData.data.login.token,
            authLoading: false,
            userId: resData.data.login.userId,
            permissionCode: resData.data.login.permissionCode,
          });
          localStorage.setItem('token', resData.data.login.token);
          localStorage.setItem('userId', resData.data.login.userId);
          localStorage.setItem(
            'permissionCode',
            resData.data.login.permissionCode
          );
          const remainingMilliseconds = 60 * 60 * 1000;
          const expiryDate = new Date(
            new Date().getTime() + remainingMilliseconds
          );
          localStorage.setItem('expiryDate', expiryDate.toISOString());
          this.setAutoLogout(remainingMilliseconds);
        }
      })
      .catch(err => {
        console.log("resive error --> ", err);
        this.setState({
          isAuth: false,
          authLoading: false,
          permissionCode: null,
          errors: err,
        });
      });
  };


  signupHandler = (event, authData) => {
    event.preventDefault();
    this.setState({ authLoading: true });
    const graphqlQuery = {
      query: `
        mutation CreateNewUser($name: String!, $phone: String!, $email: String!, $password: String! ) {
          createUser(userInput: {name: $name, telephonenumber: $phone, email: $email, password: $password }) {
            _id
            email
          }
        }
      `,
      variables: {
        name: authData.name,
        phone: authData.phone,
        email: authData.email,
        password: authData.password
      },
    };
    fetch('http://localhost:8000/graphql', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(graphqlQuery),
    })
      .then(res => res.json())
      .then(resData => {
        console.log(resData.errors);
        if (resData.errors && resData.errors[0].status === 423) {
          throw new Error(
            "Make sure the email address isn't used yet!"
          );
        }
        if (resData.errors && resData.errors[0].status === 425) {
          throw new Error(' Password too short!');
        }
        if (resData.errors && resData.errors[0].status === 426) {
          throw new Error(' Email not Good!');
        }
        if (resData.errors && resData.errors[0].status === 427) {
          throw new Error(' telephonenumber too short!');
        }
        if (
          resData.errors &&
          resData.errors[0].status !== 423 &&
          resData.errors[0].status !== 425 &&
          resData.errors[0].status !== 426 &&
          resData.errors[0].status !== 426
        ) {
          throw new Error('User creation failed!');
        }
        this.setState({
          isAuth: false,
          authLoading: false,
          successSigup: true
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          isAuth: false,
          authLoading: false,
          errors: err,
        });
      });
  };
  signupAdminHandler = (event, authData) => {
    event.preventDefault();
    this.setState({ authLoading: true });
    const formData = new FormData();
    formData.append('image', authData.imageUrl);
    console.log(authData);

    fetch('http://localhost:8000/post-image', {
      method: 'PUT',
      headers: {
        Authorization: 'Bearer ' + this.state.token
      },
      body: formData
    })
      .then(res => res.json())
      .then(fileResData => {
        if (authData.imageUrl === undefined || authData.imageUrl === null) {
          throw new Error(
            "imageUrl is null"
          );
        }
        const imageUrl = fileResData.filePath.replace('\\', '/') || 'undefined';
        console.log("imageUrl ", imageUrl);
        const graphqlQuery = {
          query: `
            mutation CreateNewAdmin($name: String!, $phone: String!, $email: String!, $imageUrl: String!, $contact: String!, $content: String!, $password: String!, $permissions: Int! ) {
              createAdmin(userInput: {name: $name, telephonenumber: $phone, email: $email, imageUrl: $imageUrl, contact: $contact, content: $content, password: $password, permissions: $permissions }) {
                _id
                email
              }
            }
          `,
          variables: {
            name: authData.name,
            phone: authData.phone,
            email: authData.email,
            imageUrl: imageUrl,
            contact: authData.contact,
            content: authData.content,
            password: authData.password,
            permissions: 2,

          },
        };

        fetch('http://localhost:8000/graphql', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(graphqlQuery),
        })
          .then(res => res.json())
          .then(resData => {
            console.log(resData.errors);
            if (resData.errors && resData.errors[0].status === 423) {
              throw new Error(
                "Make sure the email address isn't used yet!"
              );
            }
            if (resData.errors && resData.errors[0].status === 425) {
              throw new Error(' Password too short!');
            }
            if (resData.errors && resData.errors[0].status === 426) {
              throw new Error(' Email not Good!');
            }
            if (resData.errors && resData.errors[0].status === 427) {
              throw new Error(' telephonenumber too short!');
            }
            if (
              resData.errors &&
              resData.errors[0].status !== 423 &&
              resData.errors[0].status !== 425 &&
              resData.errors[0].status !== 426 &&
              resData.errors[0].status !== 426
            ) {
              throw new Error('User creation failed!');
            }
            this.setState({
              authLoading: false,
              successSigup: true
            });
          })
          .catch(err => {
            console.log(err);
            this.setState({
              authLoading: false,
              errors: err,
            });
          });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          authLoading: false,
          errors: err,
        });
      });
  };
  render() {
    let routes = (
      <Switch>
        <Route path="/home"
          exact
          render={() => <HomePage
          />} />
        <Route
          path="/booking"
          exact
          render={(props) => (
            <BookingPage
              {...props}
              userId={this.state.userId}
              isAuth={this.state.isAuth}
            />
          )}
        />
        <Route
          path="/handle"
          exact
          render={() => (
            <Handle
            />
          )}
        />
        <Route
          path="/contact"
          exact
          render={() => <Contact
          />} />
        <Route
          path="/register"
          exact
          render={props => (
            <RegisterPage
              {...props}
              onSignup={this.signupHandler}
              success={this.state.successSigup}
              successHandler={this.successHandler}
              error={this.state.errors}
              errorHandler={this.errorHandler}
            />
          )}
        />
        <Route
          path="/login"
          exact
          render={props => (
            <LoginPage
              {...props}
              onLogin={this.loginHandler}
              error={this.state.errors}
              errorHandler={this.errorHandler}
            />
          )}
        />
        <Redirect to="/home" />
      </Switch>
    );
    if (this.state.isAuth && this.state.permissionCode === 1) {
      routes = (
        <Switch>
          <Route path="/home"
            exact
            render={() => <HomePage />} />
          <Route
            path="/booking"
            exact
            render={(props) => (
              <BookingPage
                {...props}
                isAuth={this.state.isAuth}
                userId={this.state.userId}
              />
            )}
          />
          <Route
            path="/handle"
            exact
            render={() => (
              <Handle
              />
            )}
          />
          <Route
            path="/userinfor"
            exact
            render={() => (
              <ProfileUser
                userId={this.state.userId}
              />
            )}
          />
          <Route path="/contact"
            exact
            render={() => <Contact
            />} />
          <Redirect to="/home" />
        </Switch>
      );
    }
    if (this.state.isAuth === true && this.state.permissionCode === 2) {
      routes = (
        <Switch>
          <Route
            path="/admin"
            exact
            render={props => (
              <AdminPage
                {...props}
                token={this.state.token}
                userId={this.state.userId}
              />
            )}
          />
          <Route
            path="/admininfor"
            exact
            render={() => (
              <ProfileAdmin
                userId={this.state.userId}
              />
            )}
          />
          <Redirect to="/admin" />
        </Switch>
      );
    }
    if (this.state.isAuth === true && this.state.permissionCode === 3) {
      routes = (
        <Switch>
          <Route path="/signup"
            exact
            render={props => <SingUp
              {...props}
              onSignup={this.signupAdminHandler}
              loading={this.state.authLoading}
              success={this.state.successSigup}
              successHandler={this.successHandler}
              error={this.state.errors}
              errorHandler={this.errorHandler}
            />} />
          <Route
            path="/handleadmin"
            exact
            render={(props) => (
              <HandleAd
                {...props}
                userId={this.state.userId}
                token={this.state.token}
              />
            )}
          />
          <Route
            path="/main"
            exact
            render={(props) => (
              <MainOwner
                {...props}
                token={this.state.token}
              />
            )}
          />
          <Redirect to="/main" />
        </Switch>
      );

    }
    if (this.state.isAuth === true && this.state.permissionCode === 0) {
      routes = (
        <Switch>
          <Route
            path="/band"
            exact
            render={() => (
              <Band
                autoBandLogout={this.setAutoBandLogout}
              />
            )}
          />
          <Redirect to="/band" />
        </Switch>
      );

    }
    return (
      <Fragment>
        {this.state.authLoading && <Loading />}
        {this.props.location.pathname === '/home' ?
          (
            <Banner>
              <MainNavigation
                onLogout={this.logoutHandler}
                isAuth={this.state.isAuth}
                isPermis={this.state.permissionCode}
              />
            </Banner>
          ) : (
            <MainNavigation
              onLogout={this.logoutHandler}
              isAuth={this.state.isAuth}
              isPermis={this.state.permissionCode}
            />
          )}

        <Layout>
          {routes}
        </Layout>
        <Footer />

      </Fragment>
    );
  }
}

export default withRouter(App);
