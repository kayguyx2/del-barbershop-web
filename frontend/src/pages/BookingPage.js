import React, { Component } from 'react'
import styled from 'styled-components';
import { Row, Col } from 'react-bootstrap';
import moment from 'moment';

// call class
import SuccessLine from '../components/Booking/SuccessLine/SuccessLine';
import Page1 from '../components/Booking/Page_1';
import Page2 from '../components/Booking/Page_2';
import Page3 from '../components/Booking/Page_3';
import Page4 from '../components/Booking/Page_4';
import SentKey from '../components/Modal/SentKey/SentKey';


const AppWrapper = styled.div`  
    display: flex ;
    justify-content: center ;
    background-color: white;
    .left{
        left: 25px;
    }
    .right{
        right: 25px
    }
`;

const ProgressBarContainer = styled.div`
    box-shadow: 7px 7px 21px 5px rgba(0,0,0,0.21);
    width: 700px;  
    padding-left: 25px;
    padding-right: 25px;
    padding-bottom: 25px;
    margin: 5% 0;
`;

const Button = styled.button`
    margin-top: 10px;
    background-color: ${props => props.color};
    color: ${props => props.textColor};
    text-decoration: none !important;
    border : 0px;
    border-radius: 10px;
    padding: 5px;
    &:hover {
        opacity: 0.9;
        box-shadow: 2px 3px 15px -3px #000000;
        color: black;
      }
    &:focus {
        outline:0;
    }
`;
class BookingPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            percentage: 0,
            cal: 33,
            colorIcon: {
                icon1: 'SlateGrey',
                icon2: 'SlateGrey',
                icon3: 'SlateGrey'
            },
            page: {
                page_1: true,
                page_2: false,
                page_3: false,
                page_4: false,
            },
            showButton_back: false,
            showButton_next: true,
            showButton_success: false,
            startDate: null,
            showPicktime: false,
            generateTime: [],
            Admin: [],
            isLoading: false,
            slot: [],
            data: {
                date: null,
                time: null,
                adminId: null,
                name: '',
                phone: '',
                email: ''
            },
            endAdmin: null,
            activeIndexP1: null,
            activeIndexP2: null,
            showModel: false,
            errors: null,
            validname: false,
            validphone: false,
            validemail: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeTime = this.handleChangeTime.bind(this);
        this.setAdminId = this.setAdminId.bind(this);
        this.handleModel = this.handleModel.bind(this);

        // page 3
        this.handleChangeP3 = this.handleChangeP3.bind(this);
        this.handlevalidP3 = this.handlevalidP3.bind(this);

    }

    showAdmin() {
        const Admin = this.state.Admin.map(ad => ({ ...ad }))
            .filter(ad => ad._id === this.state.data.adminId)
        console.log(Admin[0]);
        // this.setState({
        //     endAdmin: Admin[0]
        // })
    }
    ////////////////////////// Page 1 //////////////////////////////
    handleChange = (date) => {
        // event.preventDefault();
        // this.setState({ authLoading: true });
        // const dateK = new Date(date.toISOString());

        const graphqlQuery = {
            query: `
                query newCallByDate($dateIn: String!){
                    callByDate(dateIn: $dateIn){
                        time
                    }
                }
          `,
            variables: {
                dateIn: date.toISOString(),
            },
        };
        fetch('http://localhost:8000/graphql', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(graphqlQuery),
        })
            .then(res => res.json())
            .then(resData => {
                this.setState({
                    startDate: date,
                    slot: resData.data.callByDate,
                    data: {
                        ...this.state.data,
                        date: date.toISOString(),
                    }
                });
                if (resData.data.callByDate.length === 0) {
                    this.setState({
                        showPicktime: false
                    })
                }
                if (resData.data.callByDate.length !== 0) {
                    this.setState({
                        showPicktime: true
                    })
                }
                this.generateTime();
                this.chackBook();
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    showPicktime: false,
                });
            });
    }

    componentDidMount() {
        this.callInformation();
    }
    handleChangeTime = (index, time) => {
        this.setState({
            data: {
                ...this.state.data,
                time: time
            },
            activeIndexP1: index
        })
    }
    chackBook = () => {
        const Booking = this.state.slot;
        const Time = this.state.generateTime;

        for (let i = 0; i < Booking.length; i++) {
            for (let j = 0; j < Time.length; j++) {
                if (Booking[i].time === Time[j]) {
                    const id_tag = Time[j]
                    const position = Time.indexOf(id_tag);
                    if (~position) Time.splice(position, 1);
                }
            }
        }

        const set_date = moment(this.state.startDate).toISOString();
        const test_date = moment().startOf('day').toISOString();
        const test_datetime = moment().add(1, 'hours').toISOString();

        if (set_date === test_date) {
            for (let j = 0; j < Time.length; j++) {
                if (test_datetime > Time[j]) {
                    const id_tag = Time[j]
                    const position = Time.indexOf(id_tag);
                    Time.splice(position, 1);
                    j--;
                }
            }
        }

        this.setState({
            generateTime: Time
        })
    }

    generateTime = () => {
        const setDate = moment(this.state.startDate);
        let setDated = moment(setDate).add(10, 'hours');
        let ArrayTime = [];
        ArrayTime.push(setDated.toISOString());
        const count = 12;
        for (let i = 1; i < count; i++) {
            setDated.add(30, 'minutes');
            ArrayTime.push(setDated.toISOString())
        }
        this.setState({
            generateTime: ArrayTime,
        })
    }
    ///////////////////////////////////// end ///////////////////////////////

    ////////////////////////////////////// page 2 ///////////////////////////
    callAdmin = () => {
        this.setState({ isLoading: true });
        const requestData = {
            query: `
                query{
                    callAdmin{
                      name
                      imageUrl
                      _id
                    }
                }
            `
        };
        fetch('http://localhost:8000/graphql', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestData),
        })
            .then(res => {
                return res.json();
            })
            .then(resData => {
                this.setState({
                    Admin: resData.data.callAdmin,
                    isLoading: false
                });

            })
            .catch(err => {
                this.setState({ isLoading: false });
                console.log(err);
            });
    }
    setAdminId = (props) => {
        this.setState({
            data: {
                ...this.state.data,
                adminId: props.adminId
            },
            activeIndexP2: props.index
        })
    }

    ////////////////////////////// end ////////////////////////

    //////////////////////////// page 3 /////////////////////////
    callInformation = () => {

        if (this.props.isAuth) {
            this.setState({ isLoading: true });
            const requestData = {
                query: `
                query CallUser($userId: ID!){
                    callUser(userId: $userId){
                      name
                      telephonenumber
                      email
                    }
                }
            `,
                variables: {
                    userId: this.props.userId,
                },
            };
            fetch('http://localhost:8000/graphql', {
                method: 'POST',
                body: JSON.stringify(requestData),
                headers: {
                    Authorization: `Bearer ${this.props.token}`,
                    'Content-Type': 'application/json',
                },
            })
                .then(res => {
                    return res.json();
                })
                .then(resData => {
                    this.setState({
                        data: {
                            ...this.state.data,
                            name: resData.data.callUser.name,
                            phone: resData.data.callUser.telephonenumber,
                            email: resData.data.callUser.email,
                        }
                    })
                })
                .catch(err => {
                    this.setState({ isLoading: false });
                    console.log(err);
                });
        }
    }
    handleChangeP3(event) {
        this.setState({
            data: {
                ...this.state.data,
                [event.target.name]: event.target.value,
            }
        });
    }

    handlevalidP3(event) {
        if (event.target.name === 'name') {
            const lang = event.target.value.length
            if (lang <= 1) {
                this.setState({
                    validname: true
                })
            } else {
                this.setState({
                    validname: false
                })
            }
        }
        if (event.target.name === 'phone') {
            const lang = event.target.value.length;
            const value = event.target.value;

            if (lang === 10 && !isNaN(value)) {
                this.setState({
                    validphone: false
                })
            } else {
                this.setState({
                    validphone: true
                })
            }
        }
        if (event.target.name === 'email') {
            // eslint-disable-next-line no-useless-escape
            const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (re.test(String(event.target.value).toLowerCase())) {
                this.setState({
                    validemail: false
                })
            } else {
                this.setState({
                    validemail: true
                })
            }
        }
    }
    /////////////////////////////// end  //////////////////////////

    ////////////////////////// P4 ////////////////////////////

    successBooking(event) {
        if (this.props.isAuth) {
            event.preventDefault();
            const dataBooking = this.state.data;
            console.log("data is :", dataBooking);

            // this.setState({ authLoading: true });
            const graphqlQuery = {
                query: `
              mutation CreateNewEvent($date: String!, $time: String!, $name: String!, $phone: String!, $email: String!, $barber: String!, $userId: String!){
                createEvent(eventInput: { date: $date, time: $time, name: $name,phone: $phone,email: $email,barber: $barber,userId: $userId}){
                  time
                  name
                  key
                }
              }
              `,
                variables: {
                    date: dataBooking.date,
                    time: dataBooking.time,
                    name: dataBooking.name,
                    phone: dataBooking.phone,
                    email: dataBooking.email,
                    barber: dataBooking.adminId,
                    userId: this.props.userId
                },
            };
            fetch('http://localhost:8000/graphql', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(graphqlQuery),
            })
                .then(res => res.json())
                .then(resData => {

                    console.log(resData);
                    this.setState({
                        showModel: true,
                        keyData: resData.data.createEvent
                    });
                })
                .catch(err => {
                    console.log(err);
                    this.setState({
                        errors: err,
                    });
                });
        } else {
            event.preventDefault();
            const dataBooking = this.state.data;
            console.log("data is :", dataBooking);

            // this.setState({ authLoading: true });
            const graphqlQuery = {
                query: `
              mutation CreateNewEvent($date: String!, $time: String!, $name: String!, $phone: String!, $email: String!, $barber: String!){
                createEvent(eventInput: { date: $date, time: $time, name: $name,phone: $phone,email: $email,barber: $barber}){
                  time
                  name
                  key
                }
              }
              `,
                variables: {
                    date: dataBooking.date,
                    time: dataBooking.time,
                    name: dataBooking.name,
                    phone: dataBooking.phone,
                    email: dataBooking.email,
                    barber: dataBooking.adminId,
                },
            };
            fetch('http://localhost:8000/graphql', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(graphqlQuery),
            })
                .then(res => res.json())
                .then(resData => {

                    console.log(resData);
                    this.setState({
                        showModel: true,
                        keyData: resData.data.createEvent
                    });
                })
                .catch(err => {
                    console.log(err);
                    this.setState({
                        errors: err,
                    });
                });
        }
    }




    ////////////////////////////// end //////////////////////


    /////////////////////////////// success ////////////////////////
    handleModel() {
        if (this.state.showModel) {
            this.setState({
                showModel: false
            })
            this.props.history.replace('/home');
        } else {
            this.setState({
                showModel: true
            })
        }

    }

    /////////////////////////////// end  //////////////////////////
    componentDidUpdate(prevProps, prevState) {
        if (this.state.percentage === 33 && prevState.percentage === 0) {
            this.setState({
                showButton_back: true,
                colorIcon: {
                    icon1: 'LimeGreen',
                    icon2: 'SlateGrey',
                    icon3: 'SlateGrey'
                },
                page: {
                    page_1: false,
                    page_2: true,
                    page_3: false,
                    page_4: false
                }
            })
        }
        if (this.state.percentage === 0 && prevState.percentage === 33) {
            this.setState({
                showButton_back: false,
                colorIcon: {
                    icon1: 'SlateGrey',
                    icon2: 'SlateGrey',
                    icon3: 'SlateGrey'
                },
                page: {
                    page_1: true,
                    page_2: false,
                    page_3: false,
                    page_4: false
                }
            })

        }
        if (this.state.percentage === 66 && prevState.percentage === 33) {
            this.setState({
                colorIcon: {
                    icon1: 'LimeGreen',
                    icon2: 'LimeGreen',
                    icon3: 'SlateGrey'
                },
                page: {
                    page_1: false,
                    page_2: false,
                    page_3: true,
                    page_4: false

                }
            })
        }
        if (this.state.percentage === 33 && prevState.percentage === 66) {
            this.setState({
                colorIcon: {
                    icon1: 'LimeGreen',
                    icon2: 'SlateGrey',
                    icon3: 'SlateGrey'
                },
                page: {
                    page_1: false,
                    page_2: true,
                    page_3: false,

                }
            })
        }
        if (this.state.percentage === 100 &&
            prevState.percentage === 66 &&
            prevState.showButton_back &&
            prevState.showButton_next) {
            this.setState({
                showButton_success: true,
                showButton_next: false,
                colorIcon: {
                    icon1: 'LimeGreen',
                    icon2: 'LimeGreen',
                    icon3: 'LimeGreen'
                },
                page: {
                    page_1: false,
                    page_2: false,
                    page_3: false,
                    page_4: true
                }
            })
        }
        if (this.state.percentage === 66 &&
            prevState.percentage === 100 &&
            prevState.showButton_back &&
            prevState.showButton_success) {
            this.setState({
                showButton_success: false,
                showButton_next: true,
                colorIcon: {
                    icon1: 'LimeGreen',
                    icon2: 'LimeGreen',
                    icon3: 'SlateGrey'
                },
                page: {
                    page_1: false,
                    page_2: false,
                    page_3: true,
                    page_4: false
                }
            })
        }

    }
    clamp = (min, value, max) => {
        return Math.min(Math.max(min, value), max);
    }
    render() {
        return (

            <AppWrapper>
                <ProgressBarContainer>
                    <SuccessLine
                        percentage={this.state.percentage}
                        clamp={this.clamp}
                        colorIcon={this.state.colorIcon}
                    />
                    {this.state.showModel &&
                        <SentKey
                            name={this.state.keyData.name}
                            sentKey={this.state.keyData.key}
                            time={this.state.keyData.time}
                            close={this.handleModel}
                        />
                    }
                    {/*---------------------- select page ------------------------------- */}
                    {this.state.page.page_1 &&
                        <Page1
                            activeIndex={this.state.activeIndexP1}
                            startDate={this.state.startDate}
                            handleChange={this.handleChange}
                            handleChangeTime={this.handleChangeTime}
                            generateTime={this.state.generateTime}
                        />
                    }
                    {this.state.page.page_2 &&
                        <Page2
                            activeIndex={this.state.activeIndexP2}
                            Admin={this.state.Admin}
                            callAdmin={this.callAdmin}
                            setAdminId={this.setAdminId}
                        />
                    }
                    {this.state.page.page_3 &&
                        (<Page3
                            userId={this.props.userId}
                            setUser={this.setFormationUser}
                            handleChange={this.handleChangeP3}
                            handlevalid={this.handlevalidP3}
                            name={this.state.data.name}
                            phone={this.state.data.phone}
                            email={this.state.data.email}
                            validname={this.state.validname}
                            validphone={this.state.validphone}
                            validemail={this.state.validemail}
                        />)
                    }
                    {this.state.page.page_4 &&
                        (<Page4
                            data={this.state.data}
                            admin={this.state.Admin}
                        />)
                    }
                    <hr />

                    {/*---------------------- buttom styled ------------------------------- */}
                    <Row>
                        <Col>
                            <div className="col text-left left">
                                {this.state.showButton_back &&
                                    <h5>
                                        <Button
                                            color="#6bccf9"
                                            textColor="rgba(255,250,250,0.8)"
                                            onClick={
                                                () => {
                                                    if (this.state.percentage === 100) {
                                                        this.setState({
                                                            percentage: this.state.percentage - this.state.cal - 1
                                                        })
                                                    } else {
                                                        this.setState({
                                                            percentage: this.state.percentage - this.state.cal
                                                        })
                                                    }
                                                }
                                            } >
                                            back
                                    </Button>
                                    </h5>
                                }
                            </div>
                        </Col>
                        <Col>
                            <div className="col text-right right">
                                {
                                    this.state.data.date &&
                                    this.state.data.time &&
                                    this.state.page.page_1 &&
                                    <h5>
                                        <Button
                                            color="#6bccf9"
                                            textColor="rgba(255,250,250,0.95)"
                                            onClick={
                                                () => {
                                                    console.log(this.state.data);

                                                    if (this.state.percentage === 66) {
                                                        this.setState({
                                                            percentage: this.state.percentage + this.state.cal + 1
                                                        })
                                                    } else {
                                                        this.setState({
                                                            percentage: this.state.percentage + this.state.cal
                                                        })
                                                    }
                                                }
                                            }>
                                            next
                                        </Button>
                                    </h5>
                                }
                                {
                                    this.state.data.adminId &&
                                    this.state.page.page_2 &&
                                    <h5>
                                        <Button
                                            color="#6bccf9"
                                            textColor="Snow"
                                            onClick={
                                                () => {
                                                    if (this.state.percentage === 66) {
                                                        this.setState({
                                                            percentage: this.state.percentage + this.state.cal + 1
                                                        })
                                                    } else {
                                                        this.setState({
                                                            percentage: this.state.percentage + this.state.cal
                                                        })
                                                    }
                                                }
                                            }>
                                            next
                                        </Button>
                                    </h5>
                                }
                                {
                                    this.state.data.name &&
                                    this.state.data.phone &&
                                    this.state.data.email &&
                                    this.state.page.page_3 &&
                                    !this.state.validname &&
                                    !this.state.validphone &&
                                    !this.state.validemail &&
                                    <h5>
                                        <Button
                                            color="#6bccf9"
                                            textColor="Snow"
                                            onClick={
                                                () => {
                                                    if (this.state.percentage === 66) {
                                                        this.setState({
                                                            percentage: this.state.percentage + this.state.cal + 1
                                                        })
                                                    } else {
                                                        this.setState({
                                                            percentage: this.state.percentage + this.state.cal
                                                        })
                                                    }
                                                }
                                            }>
                                            next
                                        </Button>
                                    </h5>
                                }
                                {this.state.showButton_success &&
                                    <h5>
                                        <Button
                                            color="#6bccf9"
                                            textColor="Snow"
                                            onClick={(e) => this.successBooking(e)}
                                        >
                                            Success
                                    </Button>
                                    </h5>
                                }
                            </div>
                        </Col>
                    </Row>



                </ProgressBarContainer>
            </AppWrapper>
        )
    }
}

export default BookingPage;