import React, { Component } from 'react'
import styled from 'styled-components';
import ErrorModal from '../components/Modal/ErrorModal/ErrorModal';
import SuccesModal from '../components/Modal/SuccessModal/SuccessModal';
import ShowDetail from '../components/Modal/ShowDetail/ShowDetail';
import Loading from '../components/Loading/loading';
const AppWrapper = styled.div`  
    display: flex;
    justify-content: center ;
    background-color: white;
    min-height: 700px;
    .left{
        left: 25px;
    }
    .right{
        right: 25px
    }

`;

const HandleContainer = styled.div`
    box-shadow: 7px 7px 21px 5px rgba(0,0,0,0.21);
    width: 700px;  
    padding-left: 25px;
    padding-right: 25px;
    padding-bottom: 25px;
    margin: 5% 0;
    background-color: white;
    .form-input-key{
        margin-top: 20px;
        text-align: center;
    }

    .icon-key{
        text-align: center;
        margin: 25px 0;
    }
    .mr-3{
        position: relative;
        border-radius: 20px;
        margin: auto;
        max-width: 450px;
        width: 10%
        height: auto;
        margin-top 20px

    }
    .title-key{
        font-family: 'Acme', sans-serif;
        display: inline-block;
    }
    .input-key{
        text-align: center;
        border: none;
        border-bottom: 4px solid rgba(0,0,0,0.8);  
        width:100%
        font-family: 'Acme', sans-serif;
        color: Crimson;
        &:focus{
            outline: 0;
            border-bottom: 4px solid #6bccf9;
            
        }
    }
`;

const Title = styled.h3`
        margin-top: 100px;
        font-family: 'Acme', sans-serif;
        text-shadow: 2px 2px 15px rgba(206,51,44,0.73);
        text-align: center;
        color: rgba(0,0,0,0.8);

`;
const Button = styled.button`
        padding: 5px;
        margin-top: 55px;
        font-family: 'Roboto', sans-serif;
        background-color: White;
        border: 2px solid ${props => props.color};
        border-radius: 8px;
        text-align: center;
        color: ${props => props.color};
        margin: 20px 10px;
        box-shadow: 1px 1px 18px 2px rgba(0,0,0,0.16);
        &:focus{
            outline: 0;
        }
        &:hover{
            color: #f5f5f5;
            background-color: ${props => props.color};
            border: 2px solid #f5f5f5;
            
        }
`;

class Handle extends Component {

    constructor(props) {
        super(props);
        this.state = {
            authLoading: false,
            key: '',
            showSuccess: false,
            showDetail: false,
            booked: null,
            err: null
        }
        this.handleCancel = this.handleCancel.bind(this);
        this.handleChangeKey = this.handleChangeKey.bind(this);

    }

    handleChangeKey(event) {

        this.setState({
            ...this.state,
            key: event.target.value,
        });
    }

    errorHandler = (event) => {
        event.preventDefault();
        this.setState({ err: null });
    };
    successHandler = (event) => {
        event.preventDefault();
        this.setState({ showSuccess: false });
    };
    showDetailHandler = () => {
        this.setState({ showDetail: false });
    };

    handleShowDetail = (key) => {
        this.setState({ authLoading: true });
        const graphqlQuery = {
            query: `
                query EventsByKey($key :String!){
                    eventsByKey(key: $key){
                    time
                    name
                    barber{
                        name
                        telephonenumber
                    }
                    }
                }
          `,
            variables: {
                key: key,
            },
        };
        fetch('http://localhost:8000/graphql', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(graphqlQuery),
        })
            .then(res => res.json())
            .then(resData => {
                console.log(resData);
                
                if (this.state.key === '') {
                    throw new Error(
                        "Empty key data "
                    );
                }
                if (resData.errors && resData.errors[0].status === 402) {
                    throw new Error(
                        "Key is incorrect"
                    );
                }
                console.log("run");
                this.setState({
                    booked: resData.data.eventsByKey[0], 
                    authLoading: false,
                    key: ''
                });

                this.setState({
                    showDetail: true
                })
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    authLoading: false,
                    err: err,
                    key: ''
                });
            });
    }
    handleCancel = (key) => {
        this.setState({ authLoading: true });
        const graphqlQuery = {
            query: `
                mutation CancelEvents($kay: String!){
                    cancelEvents(key: $kay)
                }
          `,
            variables: {
                kay: key,
            },
        };
        fetch('http://localhost:8000/graphql', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(graphqlQuery),
        })
            .then(res => res.json())
            .then(resData => {

                if (this.state.key === '') {
                    throw new Error(
                        "Empty key data "
                    );
                }
                if (resData.errors && resData.errors[0].status === 402) {
                    throw new Error(
                        "Key is incorrect"
                    );
                }
                console.log("run");

                this.setState({
                    showSuccess: true,
                    authLoading: false,
                    key: ''
                });
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    authLoading: false,
                    err: err,
                    key: ''
                });
            });
    }

    render() {
        return (
            <AppWrapper>
                {this.state.err &&
                    <ErrorModal
                        message={this.state.err.message}
                        close={this.errorHandler}
                    />}
                {this.state.showSuccess &&
                    <SuccesModal
                        message={"Completed to Remove Booking"}
                        close={this.successHandler}
                    />}
                {this.state.showDetail &&
                    <ShowDetail
                        name={this.state.booked.name}
                        time={this.state.booked.time}
                        barber={this.state.booked.barber.name}
                        phone={this.state.booked.barber.telephonenumber}
                        close={this.showDetailHandler}
                    />}
                {this.state.authLoading ? <Loading /> :
                    <HandleContainer>
                        <Title>HANDEL BOOKING</Title>
                        <div
                            className="icon-key">
                            <img
                                className="mr-3"
                                src="http://localhost:8000/images/icon/key.png"
                                alt="icon key"
                            />
                        </div>
                        <div className="form-input-key">
                            <h4 className="title-key">KEY :
                        <input
                                    className="input-key"
                                    value={this.state.key}
                                    onChange={this.handleChangeKey}
                                >
                                </input>
                            </h4>
                            <div className="Btn-handel">
                                <h5>
                                    <Button
                                        onClick={() => this.handleShowDetail(this.state.key)}
                                        color="#6bccf9">
                                        Show
                                    </Button>
                                    <Button
                                        onClick={() => this.handleCancel(this.state.key)}
                                        color="Crimson">
                                        Cancel
                                    </Button>
                                </h5>
                            </div>
                        </div>
                    </HandleContainer>
                }
            </AppWrapper>
        )
    }
}

export default Handle;
