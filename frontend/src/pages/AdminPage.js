import React, { Component } from 'react';

import styled from 'styled-components';
import AdminNavigation from '../components/AdminNavigation/AdminNavigation';
import MemberAdmin from './SectionAdmin/MemberAdmin';
import ServiceBuilder from '../containers/ServiceBuilder/ServiceBuilder';
import ListServiceAdmin from './SectionAdmin/ListServiceAdmin';
import ListBookAdmin from './SectionAdmin/ListBookAdmin';

const Styles = styled.div`
  padding-bottom: 100px;
`;
class AdminPage extends Component {
  state = {
    serviceShow: true,
    listService: false,
    listmemberShow: false,
    listBooking: false,
  };

  clickService = () => {
    this.setState({
      serviceShow: true,
      listService: false,
      listmemberShow: false,
      listBooking: false,
    });
  };

  clickListMember = () => {
    this.setState({
      serviceShow: false,
      listService: false,
      listmemberShow: true,
      listBooking: false,
    });
  };

  clickListBook = () => {
    this.setState({
      serviceShow: false,
      listService: false,
      listmemberShow: false,
      listBooking: true,
    });
  };

  clickListService = () => {
    this.setState({
      serviceShow: false,
      listService: true,
      listmemberShow: false,
      listBooking: false,
    });
  };

  render() {
    return (
      <React.Fragment>
        <Styles>
          <AdminNavigation
            clickService={this.clickService}
            clicklistmember={this.clickListMember}
            clicklistservice={this.clickListService}
            clicklistbook={this.clickListBook}
          />
          {this.state.serviceShow && (
            <ServiceBuilder
              userId={this.props.userId}
              token={this.props.token}

            />
          )}
          {this.state.listmemberShow &&
            <MemberAdmin />
          }
          {this.state.listBooking &&
            <ListBookAdmin
              userId={this.props.userId}
             />
          }
          {this.state.listService && (
            <ListServiceAdmin
              userId={this.props.userId}
              token={this.props.token}
            />
          )}
        </Styles>
      </React.Fragment>
    );
  }
}

export default AdminPage;
