import React, { Component } from 'react';
import Line from '../components/HomePage/LineSubTitie/LineSubtitile';
import Load from '../components/Loading/loading';
import { Container, Form, Row, Col, Button } from 'react-bootstrap';
import moment from 'moment';

class ProfileUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: {
                name: {
                    value: '',
                    valid: false,
                    nameValid: ''
                },
                email: {
                    value: '',
                },
                phone: {
                    value: '',
                    valid: false,
                    nameValid: ''
                },
            },
            event: [],
            loader: false
        }
        this.handleChange = this.handleChange.bind(this);
        this.cleanInput = this.cleanInput.bind(this);
        this.handleValid = this.handleValid.bind(this);
        this.handleSubmitEdit = this.handleSubmitEdit.bind(this);
    }
    componentDidMount() {
        this.fetchUser();
    }

    handleSubmitEdit(e) {
        this.setState({ loader: true });
        e.preventDefault();
        const graphqlQuery = {
            query: `
                mutation UpdateUser($id: String!, $name: String!, $phone: String! ){
                    updateUser(updateUserInput: {_id: $id, name: $name, telephonenumber: $phone}){
                    name
                    }
                }
          `,
            variables: {
                id: this.props.userId,
                name: this.state.data.name.value,
                phone: this.state.data.phone.value,

            },
        };

        fetch('http://localhost:8000/graphql', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(graphqlQuery),
        })
            .then(res => res.json())
            .then(resData => {
                this.setState({
                    loader: false
                }, this.fetchUser);
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    loader: false
                });
            });
    }
    fetchUser() {
        this.setState({ loader: true });
        const requestData = {
            query: `
                query CallUser($id: ID!){
                    callUser(userId: $id){
                        name
                        telephonenumber
                        email
                        createdEvents{
                            date
                            time
                            key
                        }
                    }
                }    
                `,
            variables: {
                id: this.props.userId
            }
        };
        fetch('http://localhost:8000/graphql', {
            method: 'POST',
            body: JSON.stringify(requestData),
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(res => {
                if (res.status !== 200 && res.status !== 201) {
                    throw new Error('Failed!');
                }
                return res.json();
            })
            .then(resData => {
                this.setState({
                    ...this.state,
                    data: {
                        ...this.state.data,
                        name: {
                            ...this.state.data.name,
                            value: resData.data.callUser.name
                        },
                        phone: {
                            ...this.state.data.phone,
                            value: resData.data.callUser.telephonenumber
                        },
                        email: {
                            ...this.state.data.email,
                            value: resData.data.callUser.email
                        }
                    },
                    event: resData.data.callUser.createdEvents,
                    loader: false
                });
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    loader: false
                });
            });
    }
    handleChange(event) {
        this.setState({
            data: {
                ...this.state.data,
                [event.target.name]: {
                    ...[event.target.name],
                    value: event.target.value.trim()
                },
            }
        });
    }
    handleValid(event) {
        if (event.target.name === 'name') {
            const lang = event.target.value.length
            if (lang <= 1) {
                this.setState({
                    data: {
                        ...this.state.data,
                        name: {
                            ...this.state.data.name,
                            valid: true,
                            nameValid: 'Must have at least 2 character'
                        }
                    }
                })
            } else {
                this.setState({
                    data: {
                        ...this.state.data,
                        name: {
                            ...this.state.data.name,
                            valid: false,
                            nameValid: ''
                        }
                    }
                })
            }
        }
        if (event.target.name === 'phone') {
            const lang = event.target.value.length;
            const value = event.target.value;

            if (lang === 10 && !isNaN(value)) {
                this.setState({
                    data: {
                        ...this.state.data,
                        phone: {
                            ...this.state.data.phone,
                            valid: false,
                            nameValid: ''
                        }
                    }
                })
            } else {
                this.setState({
                    data: {
                        ...this.state.data,
                        phone: {
                            ...this.state.data.phone,
                            valid: true,
                            nameValid: 'Must have 10 numbers'
                        }
                    }
                })
            }
        }
    }
    cleanInput(event) {
        event.preventDefault();
        this.setState({
            data: {
                ...this.state.data,
                name: {
                    value: '',
                    valid: false,
                    nameValid: ''
                },
                phone: {
                    value: '',
                    valid: false,
                    nameValid: ''
                }
            }
        })
    }
    render() {
        return (
            <div>
                {this.state.loader && <Load />}
                <Line subTitle="User Information" stylez="style-eight" style={{ marginTop: 70 }} />
                <hr />
                <Container>
                    <Form.Group as={Row} >
                        <Form.Label column sm="4" style={{ textAlign: "right" }}>Nickname</Form.Label>
                        <Col sm="6">
                            <Form.Control
                                type="text"
                                name="name"
                                value={this.state.data.name.value}
                                onChange={this.handleChange}
                                onBlur={this.handleValid}
                                isInvalid={this.state.data.name.valid}
                            />
                            <Form.Text className="text-danger" style={{ textAlign: "left" }}>
                                {this.state.data.name.nameValid}
                            </Form.Text>
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} >
                        <Form.Label column sm="4" style={{ textAlign: "right" }}>Telephone Number </Form.Label>
                        <Col sm="6" >
                            <Form.Control
                                type="text"
                                name="phone"
                                value={this.state.data.phone.value}
                                onChange={this.handleChange}
                                onBlur={this.handleValid}
                                isInvalid={this.state.data.phone.valid}
                            />
                            <Form.Text className="text-danger" style={{ textAlign: "left" }}>
                                {this.state.data.phone.nameValid}
                            </Form.Text>
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} >
                        <Form.Label column sm="4" style={{ textAlign: "right" }}>Email</Form.Label>
                        <Col sm="6">
                            <Form.Control
                                type="email"
                                name="email"
                                plaintext
                                readOnly
                                defaultValue={this.state.data.email.value}
                            />
                            <Form.Text className="text-danger" style={{ textAlign: "left" }}>
                                {this.state.data.email.nameValid}
                            </Form.Text>
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} >
                        <Col style={{ textAlign: "right" }}>
                            <Button
                                style={{ marginRight: "25px" }}
                                className="btn btn-warning"
                                onClick={this.cleanInput} >
                                Reste
                            </Button>
                            <Button
                                className="btn btn-dark"
                                onClick={this.handleSubmitEdit}>
                                success
                            </Button>
                        </Col>
                    </Form.Group>
                    <br />
                    <div className="table-responsive">
                        <table
                            className="table table-striped table-bordered"
                            style={{ textAlign: "center", lineHeight: 1.5, width: '100%' }}
                        >
                            <thead>
                                <tr className="thead-dark">
                                    <th width="5%">#</th>
                                    <th width="25%">Date</th>
                                    <th width="25%">Time</th>
                                    <th width="25%">Key</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.event.map((event, index) => {
                                        return (
                                            <tr key={index}>
                                                <td>{++index}</td>
                                                <td>{moment(Number(event.date)).format('L')}</td>
                                                <td>{moment(Number(event.time)).format('LT')}</td>
                                                <td>{event.key}</td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </Container>
            </div>
        )
    }
}

export default ProfileUser;