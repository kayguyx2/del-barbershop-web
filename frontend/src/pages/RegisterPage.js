import React, { Component } from 'react';
import { Button, Form, Row, Col } from 'react-bootstrap';
import ErrorModal from '../components/Modal/ErrorModal/ErrorModal';
import SuccesModal from '../components/Modal/SuccessModal/SuccessModal';
import styled from 'styled-components';

const AppWrapper = styled.div`  
    display: flex ;
    justify-content: center ;
    background-color: white;
    width: 100%;
    .left{
      text-align: left;
      font-weight: normal;
      color: rgba(0,0,128,0.8);
      margin-left: 15px;
    }
    .right{
        right: 25px
    }
`;

const RegisterContainer = styled.div`
    box-shadow: 7px 7px 21px 5px rgba(0,0,0,0.21);
    width: 700px;
    padding-top: 5%;
    padding-left: 5%;
    padding-right: 5%;
    padding-bottom: 50px;
    margin: 5% 0;
    text-align: center;
    font-family: 'Roboto', sans-serif;
    font-weight: bold;
    .icon-key{
      
    }
    .mr-3{
      max-height: 80px;
      width: auto;
      margin: 25px 0;
    }

    .input-style{
      text-align: right;
      @media only screen and (max-width: 580px) {
        text-align: left;
      }
    }
    .col-control{
      text-align: right;
    }
    .control-text{
      color: red,
      text-align: left;
    }
    .btn-style{
      width: 70%;
      margin: 50px auto;
    }
`;
const Styles = styled.div`
  .login-form {
    width: 100%;
    max-width: 330px;
    padding: 15px;
    margin: auto;
    height: 100%;
  }
`;
class RegisterPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: {
        name: {
          value: '',
          valid: false,
          nameValid: ''
        },
        email: {
          value: '',
          valid: false,
          nameValid: ''
        },
        password: {
          value: '',
          valid: false,
          nameValid: ''
        },
        confirm: {
          value: '',
          valid: false,
          nameValid: ''
        },
        phone: {
          value: '',
          valid: false,
          nameValid: ''
        },
      }
    }
    this.handleChange = this.handleChange.bind(this);
    this.cleanInput = this.cleanInput.bind(this);
    this.handleValid = this.handleValid.bind(this);
  }

  handleChange(event) {
    this.setState({
      data: {
        ...this.state.data,
        [event.target.name]: {
          ...[event.target.name],
          value: event.target.value.trim()
        },
      }
    });
  }

  handleValid(event) {
    console.log("target name: ", event.target.name);

    if (event.target.name === 'name') {
      const lang = event.target.value.length
      if (lang <= 1) {
        this.setState({
          data: {
            ...this.state.data,
            name: {
              ...this.state.data.name,
              valid: true,
              nameValid: 'Must have at least 2 character'
            }
          }
        })
      } else {
        this.setState({
          data: {
            ...this.state.data,
            name: {
              ...this.state.data.name,
              valid: false,
              nameValid: ''
            }
          }
        })
      }
    }
    if (event.target.name === 'phone') {
      const lang = event.target.value.length;
      const value = event.target.value;

      if (lang === 10 && !isNaN(value)) {
        this.setState({
          data: {
            ...this.state.data,
            phone: {
              ...this.state.data.phone,
              valid: false,
              nameValid: ''
            }
          }
        })
      } else {
        this.setState({
          data: {
            ...this.state.data,
            phone: {
              ...this.state.data.phone,
              valid: true,
              nameValid: 'Must have 10 numbers'
            }
          }
        })
      }
    }
    if (event.target.name === 'email') {
      // eslint-disable-next-line no-useless-escape
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (re.test(String(event.target.value).toLowerCase())) {
        this.setState({
          data: {
            ...this.state.data,
            email: {
              ...this.state.data.email,
              valid: false,
              nameValid: ''
            }
          }
        })
      } else {
        // .
        this.setState({
          data: {
            ...this.state.data,
            email: {
              ...this.state.data.email,
              valid: true,
              nameValid: 'Looks like not an email'
            }
          }
        })
      }
    }

    // password 
    if (event.target.name === 'password') {
      const lang = event.target.value.length;
      if (lang > 5) {
        this.setState({
          data: {
            ...this.state.data,
            password: {
              ...this.state.data.password,
              valid: false,
              nameValid: ''
            }
          }
        })
      } else {
        this.setState({
          data: {
            ...this.state.data,
            password: {
              ...this.state.data.password,
              valid: true,
              nameValid: 'Must have at least 6 characters or numbers'
            }
          }
        })
      }
      const confirm = this.state.data.confirm.value;
      const pass = event.target.value;
      if (confirm === pass) {
        this.setState({
          data: {
            ...this.state.data,
            confirm: {
              ...this.state.data.confirm,
              valid: false,
              nameValid: ''
            }
          }
        })
      }
    }
    if (event.target.name === 'confirm') {
      const pass = this.state.data.password.value;
      const lang = event.target.value.length;
      if (event.target.value !== pass) {
        this.setState({
          data: {
            ...this.state.data,
            confirm: {
              ...this.state.data.confirm,
              valid: true,
              nameValid: 'Password and confirm password do not match'
            }
          }
        })
      } else if (lang < 6) {
        this.setState({
          data: {
            ...this.state.data,
            confirm: {
              ...this.state.data.confirm,
              valid: true,
              nameValid: 'Must have at least 6 characters or numbers'
            }
          }
        })
      } else {
        this.setState({
          data: {
            ...this.state.data,
            confirm: {
              ...this.state.data.confirm,
              valid: false,
              nameValid: ''
            }
          }
        })
      }
    }
  }
  cleanInput(event) {
    event.preventDefault();
    this.setState({
      data: {
        name: {
          value: '',
          valid: false
        },
        email: {
          value: '',
          valid: false
        },
        password: {
          value: '',
          valid: false
        },
        confirm: {
          value: '',
          valid: false
        },
        phone: {
          value: '',
          valid: false
        },
      }
    })

  }
  render() {
    return (
      <AppWrapper>
        <RegisterContainer>
          <Styles>
            {this.props.error &&
              <ErrorModal
                message={this.props.error.message}
                close={this.props.errorHandler}
              />
            }
            {this.props.success &&
              <SuccesModal
                message={"You have finished registering."}
                close={this.props.successHandler}
              />}
            <Form
              onSubmit={e =>
                this.props.onSignup(e, {
                  name: this.state.data.name.value,
                  phone: this.state.data.phone.value,
                  email: this.state.data.email.value,
                  password: this.state.data.password.value,
                })
              }
            >

              <div
                className="icon-key">
                <img
                  className="mr-3"
                  src="http://localhost:8000/images/icon/test.png"
                  alt="icon logo"
                />
              </div>
              <h3>Registration Page</h3>
              <br />
              <div className="input-style">
                <Form.Group as={Row} >
                  <Form.Label column sm="4">Nickname*</Form.Label>
                  <Col sm="6">
                    <Form.Control
                      type="text"
                      name="name"
                      value={this.state.data.name.value}
                      onChange={this.handleChange}
                      onBlur={this.handleValid}
                      isInvalid={this.state.data.name.valid}
                    />
                    <Form.Text className="text-danger" style={{ textAlign: "left" }}>
                      {this.state.data.name.nameValid}
                    </Form.Text>
                  </Col>
                </Form.Group>
                <Form.Group as={Row} >
                  <Form.Label column sm="4">Email</Form.Label>
                  <Col sm="6">
                    <Form.Control
                      type="email"
                      name="email"
                      value={this.state.data.email.value}
                      onChange={this.handleChange}
                      onBlur={this.handleValid}
                      isInvalid={this.state.data.email.valid}
                    />
                    <Form.Text className="text-danger" style={{ textAlign: "left" }}>
                      {this.state.data.email.nameValid}
                    </Form.Text>
                  </Col>
                </Form.Group>
                <Form.Group as={Row} >
                  <Form.Label column sm="4">password</Form.Label>
                  <Col sm="6">
                    <Form.Control
                      type="password"
                      name="password"
                      autoComplete="off"
                      value={this.state.data.password.value}
                      onChange={this.handleChange}
                      onBlur={this.handleValid}
                      isInvalid={this.state.data.password.valid}
                    />
                    <Form.Text className="text-danger" style={{ textAlign: "left" }}>
                      {this.state.data.password.nameValid}
                    </Form.Text>
                  </Col>
                </Form.Group>
                <Form.Group as={Row} >
                  <Form.Label column sm="4">Confirm Password:</Form.Label>
                  <Col sm="6">
                    <Form.Control
                      type="password"
                      name="confirm"
                      autoComplete="off"
                      value={this.state.data.confirm.value}
                      onChange={this.handleChange}
                      onBlur={this.handleValid}
                      isInvalid={this.state.data.confirm.valid}
                    />
                    <Form.Text className="text-danger" style={{ textAlign: "left" }}>
                      {this.state.data.confirm.nameValid}
                    </Form.Text>
                  </Col>
                </Form.Group>
                <Form.Group as={Row} >
                  <Form.Label column sm="4">Telephone Number:</Form.Label>
                  <Col sm="6">
                    <Form.Control
                      type="text"
                      name="phone"
                      value={this.state.data.phone.value}
                      onChange={this.handleChange}
                      onBlur={this.handleValid}
                      isInvalid={this.state.data.phone.valid}
                    />
                    <Form.Text className="text-danger" style={{ textAlign: "left" }}>
                      {this.state.data.phone.nameValid}
                    </Form.Text>
                  </Col>
                </Form.Group>
                <Row >
                  <Col className="col-control">
                    <Button
                      style={{ marginRight: "25px" }}
                      className="btn btn-warning"
                      onClick={this.cleanInput} >
                      Clean
                    </Button>
                    <Button className="btn btn-dark" type="submit">
                      Submit
                  </Button>
                  </Col>
                </Row>

              </div>
            </Form>
          </Styles>
        </RegisterContainer>
      </AppWrapper>

    );
  }
}

export default RegisterPage;
