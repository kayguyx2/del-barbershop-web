import React, { Component } from 'react'
import Line from '../../components/HomePage/LineSubTitie/LineSubtitile';
import { Container } from 'react-bootstrap';

class HandleAdmin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: [],
            loader: false,
            err: null
        }
    }
    componentDidMount() {
        this.fetchUser();
    }

    fetchUser() {
        this.setState({ loader: true });
        const requestData = {
            query: `
                  query {
                    callAdmin{
                        _id
                        name
                        telephonenumber
                        email
                    }
                  }
                  
                `,
        };
        fetch('http://localhost:8000/graphql', {
            method: 'POST',
            body: JSON.stringify(requestData),
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(res => {
                if (res.status !== 200 && res.status !== 201) {
                    throw new Error('Failed!');
                }
                return res.json();
            })
            .then(resData => {
                const users = resData.data.callAdmin;
                this.setState({
                    user: users,
                    loader: false
                });
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    loader: false
                });
            });
    }


    delUser = (e, userId) => {
        e.preventDefault();
        this.setState({ loader: true });
        const graphqlQuery = {
            query: `
                mutation($userID: ID!){
                    delUser(userInput: $userID)
                }
        `,
            variables: {
                userID: userId.id
            },
        };

        fetch('http://localhost:8000/graphql', {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${this.props.token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(graphqlQuery),
        })
            .then(res => res.json())
            .then(resData => {
                this.setState({
                    loader: false
                }, this.fetchUser);

            })
            .catch(err => {
                this.setState({
                    loader: false,
                    err: err
                });
            });

    }
    render() {
        return (
            <Container>
                <Line subTitle="Management User" stylez="style-eight" />
                <hr />
                <div className="table-responsive">
                    <table
                        className="table table-striped table-bordered"
                        style={{ textAlign: "center", lineHeight: 2, width: '100%' }}
                    >
                        <thead>
                            <tr className="thead-dark">
                                <th width="5%">#</th>
                                <th width="30%">Name</th>
                                <th width="30%">Telephone No.</th>
                                <th width="25%">E-mail</th>
                                <th width="10%">Remove Admin</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.user.map((user, index) => {
                                    console.log(index);

                                    return (
                                        <tr key={index}>
                                            <td>{++index}</td>
                                            <td>{user.name}</td>
                                            <td>{user.telephonenumber}</td>
                                            <td>{user.email}</td>
                                            <td>
                                                <button
                                                    className="btn btn-danger btn-sm"
                                                    onClick={(e) => this.delUser(e, {
                                                        id: user._id
                                                    })}>Remove
                                        </button>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                </div>

            </Container>
        )
    }
}

export default HandleAdmin