import React, { Component } from 'react'
import Line from '../../components/HomePage/LineSubTitie/LineSubtitile';
import Load from '../../components/Loading/loading';
import moment from 'moment';

class Summary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: false,
            amountService: '-',
            totalservice: '-',
            amountBooking: '-'
        }
    }

    componentDidMount() {
        this.fetchService();
        this.fetchBook();
    }

    fetchService() {
        this.setState({ isLoading: true });
        const requestData = {
            query: `
                query {
                    services{
                      services{
                        _id
                        generation
                        haircut
                        shave
                        totalprice
                        creator{
                          _id
                          name
                        }
                      }
                      totalservice
                    }
                  }
                `,
        };
        fetch('http://localhost:8000/graphql', {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${this.props.token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestData),
        })
            .then(res => {
                if (res.status !== 200 && res.status !== 201) {
                    throw new Error('Failed!');
                }
                return res.json();
            })
            .then(resData => {
                this.setState({
                    amountService: resData.data.services.services
                        .map(ser => ({
                            ...ser,
                        }))
                        .reduce((sum,number) => {
                            return sum + (Number(number['totalprice']) || 0)
                        },0),
                    totalservice: resData.data.services.totalservice,
                    isLoading: false
                });
            })
            .catch(err => {
                this.setState({ isLoading: false });
                console.log(err);
            });
    }
    fetchBook() {

        this.setState({ isLoading: true });
        const requestData = {
            query: `
                query{
                    events{
                    date
                    time
                    name
                    phone
                    key
                    barber{
                        _id
                        name
                    } 
                    }
                }
                `,
        };
        fetch('http://localhost:8000/graphql', {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${this.props.token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestData),
        })
            .then(res => {
                if (res.status !== 200 && res.status !== 201) {
                    throw new Error('Failed!');
                }
                return res.json();
            })
            .then(resData => {

                this.setState({
                    amountBooking: resData.data.events.length,
                    isLoading: false
                });
            })
            .catch(err => {
                console.log(err);
                this.setState({ isLoading: false });

            });
    }
    formatNumber = (num) => {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
      }
    render() {
        return (
            <div>
                {this.state.loader && <Load />}
                <Line subTitle="The Revenue Summary" stylez="style-eight" style={{ marginTop: 70 }} />
                <div className="container" style={{ marginTop: 50 }}>
                    <div className="table-responsive">
                        <table
                            className="table table-striped table-bordered"
                            style={{ textAlign: "center", lineHeight: 2, width: '100%' }}
                        >
                            <thead>
                                <tr className="thead-dark">
                                    <th width="5%">#</th>
                                    <th width="40%">Title</th>
                                    <th width="25%">Duration</th>
                                    <th width="15%">Amount</th>
                                    <th width="15%">Total Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th >1</th>
                                    <th >Total monthly income</th>
                                    <th >{`${moment().startOf('month').format('L')} - ${moment().format('L')}`}</th>
                                    <th >{this.state.totalservice}</th>
                                    <th >{this.formatNumber(this.state.amountService)+' THB .'}</th>
                                </tr>
                                <tr>
                                    <th >2</th>
                                    <th >Total monthly booking</th>
                                    <th >{`${moment().startOf('month').format('L')} - ${moment().format('L')}`}</th>
                                    <th >{this.state.amountBooking}</th>
                                    <th >-</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default Summary;