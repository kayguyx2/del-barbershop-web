import React, { Component } from 'react'
import styled from 'styled-components';
import ChartsBook from '../../components/Owner/ChartsBook/ChartsBook';
import ChartsService from '../../components/Owner/ChartsService/ChaersService';
import ChartsIncome from '../../components/Owner/ChartsIncome/ChartsIncome';
import Line from '../../components/HomePage/LineSubTitie/LineSubtitile';
// call css 
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const Styled = styled.div`
    .dashbord-booking{
       background-color: #F0F8FF;
       border-radius: 10px;
    }
    
    .date{
        margin-top: 20px;
        text-align: right;
        font-size: 15px;
    }
    .title-book{
        text-align: center;
    }
`;
class Dashbord extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    
    render() {
        return (
            <Styled>
                <div >
                <Line subTitle="Dashbord" stylez="style-eight" style={{ marginTop: 70 }} />
                    <Row className="justify-content-md-center">
                        <Col md={5} sm={12} className="dashbord-booking">
                            <ChartsBook />
                        </Col>
                        <Col md={{ span: 5, offset: 1 }} sm={12} className="dashbord-booking">
                            <ChartsService />
                        </Col>
                    </Row>
                    <br />
                    <Row className="justify-content-md-center">
                        <Col sm={12} md={11} className="dashbord-booking">
                            <ChartsIncome />
                        </Col>
                    </Row>
                    <br />
                    <br />
                </div>


            </Styled>
        )
    }
}

export default Dashbord;