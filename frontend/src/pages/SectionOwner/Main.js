import React, { Component } from 'react'
import Navber from '../../components/NavOwner/NavOwner'

// call class 
import Dashbord from './Dashbord';
import Summary from './Summary';
import Member from './ListMember';


class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Navigation: {
                dashbord: true,
                summary: false,
                member: false,
            },
            totalservice: null,
            allBarber: [],
            sumEvent: [],
            isLoading: false
        }
        this.clickDashbord = this.clickDashbord.bind(this);
        this.clickSummary = this.clickSummary.bind(this);
        this.clicklistMember = this.clicklistMember.bind(this);
    }

    componentDidMount() {
        this.fetchService();
    }

    fetchService() {
        this.setState({ isLoading: true });
        const requestData = {
            query: `
                query {
                    services{
                      services{
                        _id
                        generation
                        haircut
                        shave
                        totalprice
                        creator{
                          _id
                          name
                        }
                      }
                      totalservice
                    }
                  }
                `,
        };
        fetch('http://localhost:8000/graphql', {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${this.props.token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestData),
        })
            .then(res => {
                if (res.status !== 200 && res.status !== 201) {
                    throw new Error('Failed!');
                }
                return res.json();
            })
            .then(resData => {
                this.setState({
                    data: resData.data.services.services
                        .map(ser => ({
                            ...ser,
                        })),
                    totalservice: resData.data.services.totalservice,
                    isLoading: false
                });
            })
            .catch(err => {
                this.setState({ isLoading: false });
                console.log(err);
            });
    }

    clickDashbord() {
        this.setState({
            ...this.state,
            Navigation: {
                ...this.state.Navigation,
                dashbord: true,
                summary: false,
                member: false,
            }
        })
    }
    clickSummary() {
        this.setState({
            ...this.state,
            Navigation: {
                ...this.state.Navigation,
                dashbord: false,
                summary: true,
                member: false,
            }
        })
    }
    clicklistMember() {
        this.setState({
            ...this.state,
            Navigation: {
                ...this.state.Navigation,
                dashbord: false,
                summary: false,
                member: true,
            }
        })
    }

    render() {
        return (
            <React.Fragment>
                <Navber
                    dashbordStlye={this.state.Navigation.dashbord ? "btn-a" : "btn-c"}
                    summaryStlye={this.state.Navigation.summary ? "btn-a" : "btn-c"}
                    memberStlye={this.state.Navigation.member ? "btn-a" : "btn-c"}
                    clickDashbord={this.clickDashbord}
                    clickSummary={this.clickSummary}
                    clicklistMember={this.clicklistMember}
                />
                {this.state.Navigation.dashbord && <Dashbord />}
                {this.state.Navigation.summary && 
                    <Summary
                        token={this.props.token}
                />}
                {this.state.Navigation.member && <Member />} 
                
            </React.Fragment>

        )
    }
}

export default Main;