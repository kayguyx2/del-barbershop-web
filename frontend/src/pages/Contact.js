import React, { Component } from 'react';
import Line from '../components/HomePage/LineSubTitie/LineSubtitile';

import styled from 'styled-components';
import MapContainer from './Map'
import { Container, Row, Col } from 'react-bootstrap';



const WarpperGallery = styled.div`
    text-align: center;
    font-family: 'Acme', sans-serif;

    .title-contact{
        padding-top: 30px;
    }
    .content-contact{
        margin-top: 30px;
        text-align: left;
        font-family: 'Open Sans', sans-serif;
    }

    .pad{
        margin-left: 50px;
        display: inline-block;
    }
    .content-contact-detail{
        margin-left: 5px;
        color: rgba(0,0,0,0.7)
    }

    .pic{
        text-align: center;
        margin-bottom: 25px;
    }
    .mr-3{
        
        position: relative;
        border-radius: 20px;
        margin: auto;
        max-width: 450px;
        width: 100%
        height: auto;
        margin-top 20px

    }
`;


class Contact extends Component {
    static defaultProps = {
        center: {
            lat: 18.8035908,
            lng: 98.9740793
        },
        zoom: 18
    };
    // d18.8035908!4d98.9740793?
    render() {
        return (
            <WarpperGallery 
            >
                <div className="title-contact">
                    <Line subTitle="GET IN TOUCH WITH" stylez="style-eight" />
                </div>
                <div className="content-contact">
                    <Container>
                        <Row>
                            <Col sm={6}>
                                <MapContainer />
                            </Col>
                            <Col sm={6}>
                                <div className="content-contact-detail">
                                    <div className="pic">
                                        <img
                                            className="mr-3"
                                            src="http://localhost:8000/images/addess/01.jpg"
                                            alt="History img"
                                        />
                                    </div>
                                    <h6><i className="fas fa-map-pin"></i>{" "}Addess</h6>
                                    <p className="pad">
                                        Barber Crew ถนนสันติสุข ตำบลช้างเผือก
                                        อำเภอเมืองเชียงใหม่ เชียงใหม่ 50300
                                        เทศบาลนครเชียงใหม่ 50300<br />
                                        <i className="fas fa-mobile-alt"></i>{"    "}080 845 4381
                                    </p>
                                    <h6><i className="fas fa-mobile-alt"></i>{" "}Phone Number</h6>
                                    <p className="pad">
                                        080 845 4381
                                    </p>
                                    <h6><i className="fab fa-facebook-square"></i>{" "}Facebook</h6>
                                    <p className="pad">
                                        <a
                                            className="link-face"
                                            href="https://www.facebook.com/BARBERCREWCNX/"
                                            target="_blank"
                                            rel='noreferrer noopener'>
                                            facebook.com/BARBERCREWCNX/
                                        </a>
                                    </p>
                                    <h6><i className="far fa-clock"></i>{" "}Time Open</h6>
                                    <p className="pad">
                                        10:00 AM - 16:00 PM
                                    </p>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </WarpperGallery>
        )
    }
}

export default Contact;