import React, { Component } from 'react';
// import component
import styled from 'styled-components';
// import PlayYoutube from '../components/HomePage/PlayYoutube/PlayYoutube';
import TeamBarber from '../components/HomePage/Team/Team';
import Intro from '../components/HomePage/History/History';
import Line from '../components/HomePage/LineSubTitie/LineSubtitile';
import Clock from '../components/HomePage/Clock/Clock';
import Social from '../components/HomePage/SocialContract/SocialContract';
import Garlly from '../components/Gallery/CardGallery/CardGallery';
import { Row, Col } from 'react-bootstrap';

const Styles = styled.div`
  
  .title {
    font-size: 3rem;
    margin-top: 50px;
  }
  .subtitle {
    font-size: 1.5rem;
  }
  
  .youtube {
    margin-top: 25px;
    border-radius: 15px;
    box-shadow: 0 16px 24px 2px rgba(0, 0, 0, 0.14),
        0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2);
  }
  .history {
    margin-top: 25px;
    border-radius: 15px;
    box-shadow: 0 16px 24px 2px rgba(0, 0, 0, 0.14),
        0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2);
  }
  .pricelist{
    margin-top: 25px;
    display: block;
    margin-left: auto;
    margin-right: auto;
    border-radius: 15px;
    box-shadow: 0 16px 24px 2px rgba(0, 0, 0, 0.14),
      0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2);
  }
  .topic{
    padding-top: 25px;
  }
  .t2{
    margin: auto;
    width: 100%;
    margin-top: 25px
    text-align: center;
  }
  .stley-line{
    font-family: 'Acme', sans-serif;
  }
  hr.style-two {
    border: 0;
    height: 0.5;
    border-top: 1px solid rgba(0, 0, 0, 0.5);
    border-bottom: 1px solid rgba(255, 255, 255, 0.3);
}
`;


class HomePage extends Component {

  state = {
    Admin: [],
    isLoading: false
  }

  callAdmin = () => {
    this.setState({ isLoading: true });
    const requestData = {
      query: `
            query{
                callAdmin{
                  name
                  imageUrl
                }
            }
        `
    };
    fetch('http://localhost:8000/graphql', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestData),
    })
      .then(res => {
        return res.json();
      })
      .then(resData => {
        this.setState({
          Admin: resData.data.callAdmin,
          isLoading: false
        });
      })
      .catch(err => {
        this.setState({ isLoading: false });
        console.log(err);
      });
  }

  componentDidMount() {
    this.callAdmin();
  }


  render() {


    return (

      <Styles>
          <div className="topic">
            <Row>
              <Col >
                <Social instagram=" instagram" />
              </Col>
              <Col>
                <Clock />
              </Col>
            </Row>
          </div>
          <div className="stley-line">
            <Line subTitle="COME TO BARBERCREW" stylez="style-eight" />
          </div>
          <Intro />
          <Garlly />
          
          <TeamBarber />
          {/* <PlayYoutube /> */}


      </Styles>
    );
  }
}

export default HomePage;
