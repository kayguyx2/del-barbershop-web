import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import styled from 'styled-components';
import ErrorModal from '../components/Modal/ErrorModal/ErrorModal';

const AppWrapper = styled.div`  
    display: flex ;
    justify-content: center ;
    background-color: white;
    width: 100%;
    .left{
      text-align: left;
      font-weight: normal;
      color: rgba(0,0,128,0.8);
      margin-left: 15px;
    }
    .right{
        right: 25px
    }
`;

const LoginContainer = styled.div`
    box-shadow: 7px 7px 21px 5px rgba(0,0,0,0.21);
    width: 700px;
    padding-top: 5%;
    padding-left: 5%;
    padding-right: 5%;
    padding-bottom: 50px;
    margin: 5% 0;
    text-align: center;
    font-family: 'Roboto', sans-serif;
    font-weight: bold;
    .icon-key{
      
    }
    .mr-3{
      max-height: 80px;
      width: auto;
      margin: 25px 0;
    }

    .input-style{
      width: 70%;
      margin: 0 auto;
    }
    .btn-style{
      width: 70%;
      margin: 50px auto;
    }
`;
class LoginPage extends Component {


  constructor(props) {
    super(props);
    this.emailEl = React.createRef();
    this.passwordEl = React.createRef();
  }
  render() {

    return (
      <AppWrapper>
        <LoginContainer>
          {this.props.error &&
            <ErrorModal
              message={this.props.error.message}
              close={this.props.errorHandler}
            />
          }
            <Form
              className="login-form"
              onSubmit={e =>
                this.props.onLogin(e, {
                  email: this.emailEl.value,
                  password: this.passwordEl.value,
                })
              }
            >
              <div
                className="icon-key">
                <img
                  className="mr-3"
                  src="http://localhost:8000/images/icon/barber-pole.png"
                  alt="icon logo"
                />
              </div>
              <h3>Login Page</h3>
              <br />
              <FormGroup>
                <Label >E-mail</Label>
                <div className="input-style">
                  <Input
                    type="email"
                    placeholder="E-mail"
                    innerRef={email => (this.emailEl = email)}
                  />
                </div>

              </FormGroup>
              <FormGroup>
                <Label >Password</Label>
                <div className="input-style">
                  <Input
                    type="password"
                    autoComplete="off"
                    placeholder="Password"
                    innerRef={passwordElx => (this.passwordEl = passwordElx)}
                  />
                  
                </div>
                
              </FormGroup>
              <Button className="btn-lg btn-dark btn-block btn-style" type="submit">
                Log in
              </Button>
            </Form>
        </LoginContainer>
      </AppWrapper>
    );
  }
}

export default LoginPage;
