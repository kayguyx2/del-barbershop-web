import React from 'react';
import { Map, Marker, GoogleApiWrapper } from 'google-maps-react';
const mapStyles = {
    width: '80%',
    height: '80%',
};
const MapContainer = (props) => (
    
    <Map
        google={props.google}
        zoom={18}
        style={mapStyles}
        initialCenter={{ lat: 18.8035908, lng: 98.9740793 }}
    >
        <Marker
            position={{
                lat: 18.8035908,
                lng: 98.9740793
            }}
            title={'BarberCrew'}
        />
    </Map>
)



export default GoogleApiWrapper({
    apiKey: 'AIzaSyBcMgvuctPXDZ3uVC2Vz9G3KK8utvfQTqs'
})(MapContainer)

//AIzaSyD5hGHZ6XS2f3vPgaAc7sgO1K9xd18OSGY

//AIzaSyBcMgvuctPXDZ3uVC2Vz9G3KK8utvfQTqs