import React, { Component } from 'react';
import Line from '../../components/HomePage/LineSubTitie/LineSubtitile';
import Load from '../../components/Loading/loading';

class MemberAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      userID: '',
      name: '',
      telephonenumber: '',
      email: '',
      showEdit: false,
      loader: false,
      err: null
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmitEdit = this.handleSubmitEdit.bind(this);
  }


  componentDidMount() {
    this.fetchUser();
  }

  fetchUser() {
    this.setState({ loader: true });
    const requestData = {
      query: `
              query {
                users {
                    _id
                    name
                    telephonenumber
                    email
                    permissions
                }
              }
              
            `,
    };
    fetch('http://localhost:8000/graphql', {
      method: 'POST',
      body: JSON.stringify(requestData),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error('Failed!');
        }
        return res.json();
      })
      .then(resData => {
        const { users } = resData.data;
        console.log(users);
        this.setState({
          users: users,
          loader: false
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          loader: false
        });
      });
  }
  handleSubmitEdit(e) {
    this.setState({ loader: true });
    e.preventDefault();
    const graphqlQuery = {
      query: `
      mutation UpdateUser($id: String!, $name: String!, $phone: String! ){
        updateUser(updateUserInput: {_id: $id, name: $name, telephonenumber: $phone}){
          name
        }
      }
      `,
      variables: {
        id: this.state.userID,
        name: this.state.name,
        phone: this.state.telephonenumber,

      },
    };

    fetch('http://localhost:8000/graphql', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(graphqlQuery),
    })
      .then(res => res.json())
      .then(resData => {
        this.setState({
          userID: '',
          name: '',
          telephonenumber: '',
          showEdit: false,
          loader: false
        }, this.fetchUser);
      })
      .catch(err => {
        console.log(err);
        this.setState({
          userID: '',
          name: '',
          telephonenumber: '',
          showEdit: false,
          loader: false
        });
      });
  }

  userBlock = (e, userId) => {
    e.preventDefault();
    this.setState({ loader: true });
    const graphqlQuery = {
      query: `
        mutation($userID: ID!){
          blockUser(userID: $userID)
        }
    `,
      variables: {
        userID: userId.id
      },
    };

    fetch('http://localhost:8000/graphql', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(graphqlQuery),
    })
      .then(res => res.json())
      .then(resData => {
        console.log("resData :", resData);

        this.setState({
          loader: false
        }, this.fetchUser);

      })
      .catch(err => {
        this.setState({
          loader: false,
          err: err
        });
      });

  }
  userUnBlock = (e, userId) => {
    e.preventDefault();
    this.setState({ loader: true });
    const graphqlQuery = {
      query: `
        mutation($userID: ID!){
          unblockUser(userID: $userID)
        }
    `,
      variables: {
        userID: userId.id
      },
    };

    fetch('http://localhost:8000/graphql', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(graphqlQuery),
    })
      .then(res => res.json())
      .then(resData => {
        this.setState({ loader: false }, this.fetchUser);

      })
      .catch(err => {
        this.setState({
          loader: false,
          err: err
        });
      });
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }
  handleUpdate = (userId = null, name = null, Telephone = null) => {
    this.setState({
      userID: userId,
      name: name,
      telephonenumber: Telephone,
      showEdit: !this.state.showEdit
    })
  }
  render() {
    return (
      <React.Fragment>
        {this.state.loader && <Load />}
        <Line subTitle="Your Member List" stylez="style-eight" style={{ marginTop: 70 }} />
        <div className="container" style={{ marginTop: 50 }}>
          {this.state.showEdit && (
            <form onSubmit={this.handleSubmitEdit}>
              <div className="row">
                <div className="col-8">
                  <div className="form-row">
                    <div className="col-4">
                      <input type="text" name="name" className="form-control" placeholder="Name" onChange={this.handleChange} value={this.state.name} />
                    </div>
                    <div className="col-6">
                      <input type="text" name="telephonenumber" className="form-control" placeholder="Telephone No." onChange={this.handleChange} value={this.state.telephonenumber} />
                    </div>
                    <div className="col">
                      <button className="btn btn-info" > Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          )}
          <hr />
          <div className="table-responsive">
            <table
              className="table table-striped table-bordered"
              style={{ textAlign: "center", lineHeight: 2.5, width: '100%' }}
            >
              <thead>
                <tr className="thead-dark">
                  <th width="5%">#</th>
                  <th width="25%">Name</th>
                  <th width="25%">Telephone No.</th>
                  <th width="25%">E-mail</th>
                  <th width="10%">Edit</th>
                  <th width="10%">Block</th>
                </tr>
              </thead>
              <tbody>

                {
                  this.state.users.map((user, index) => {
                    return (
                      <tr key={index}>
                        <td>{++index}</td>
                        <td>{user.name}</td>
                        <td>{user.telephonenumber}</td>
                        <td>{user.email}</td>
                        <td>
                          <button
                            className="btn btn-warning btn-sm"
                            onClick={() => this.handleUpdate(user._id, user.name, user.telephonenumber)}>Edit</button></td>
                        {user.permissions === 1 ? (
                          <td>
                            <button
                              className="btn btn-danger btn-sm"
                              onClick={(e) => this.userBlock(e, {
                                id: user._id
                              })}>Block
                          </button>
                          </td>
                        ) : (
                            <td>
                              <button
                                className="btn btn-danger btn-sm"
                                onClick={(e) => this.userUnBlock(e, {
                                  id: user._id
                                })}>Unblock
                          </button>
                            </td>
                          )}

                      </tr>
                    )
                  })
                }
              </tbody>
            </table>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default MemberAdmin;
