import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import styled from 'styled-components';

import Loading from '../../components/Loading/loading';
import Moment from 'react-moment';
import Line from '../../components/HomePage/LineSubTitie/LineSubtitile';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

//Time start Of Month  and end Of Month

const moment = require('moment');
const startOfMonth = moment().startOf('month').format('YYYY-MM-DD hh:mm');
// const endOfMonth   = moment().endOf('month').format('YYYY-MM-DD hh:mm');

const Styles = styled.div`
  .title {
    margin-top: 25px;
    display: inline-block;
    position: relative;
    text-align: center;
    font-family: Roboto;
  }
  .total-price {
    text-decoration: underline;
    margin-top: 25px;
    text-align: left;
  }

  .text-date {
    text-align: right;
  }
  .create-by {
    text-align: right;
  }
`;

class ListServiceAdmin extends Component {
  state = {
    service: [],
    totalservice: 0,
    isLoading: false,
    err: null
  };

  componentDidMount() {
    this.fetchService();
  }

  fetchService() {
    this.setState({ isLoading: true });
    const requestData = {
      query: `
            query {
                services{
                  services{
                    _id
                    generation
                    haircut
                    shave
                    totalprice
                    creator{
                      _id
                      name
                    }
                  }
                  totalservice
                }
              }
            `,
    };
    fetch('http://localhost:8000/graphql', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${this.props.token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestData),
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error('Failed!');
        }
        return res.json();
      })
      .then(resData => {
        this.setState({
          service: resData.data.services.services
            .map(ser => ({
              ...ser,
            }))
            .filter(ser => ser.creator._id === this.props.userId),
          totalservice: resData.data.services.totalservice,
          isLoading: false
        });
      })
      .catch(err => {
        this.setState({ isLoading: false });
        console.log(err);
      });
  }


  delServer = (e, userId) => {
    e.preventDefault();
    this.setState({ loader: true });
    const graphqlQuery = {
      query: `
        mutation($userID: ID!){
          delService(serviceID: $userID)
        }
    `,
      variables: {
        userID: userId.id
      },
    };

    fetch('http://localhost:8000/graphql', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${this.props.token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(graphqlQuery),
    })
      .then(res => res.json())
      .then(resData => {
        console.log("resData :", resData);
        this.setState({
          loader: false
        }, this.fetchService);

      })
      .catch(err => {
        this.setState({
          loader: false,
          err: err
        });
      });

  }
  render() {
    const service = this.state.service.map(ser => ser);
    const total = this.state.service.map(t => t.totalprice).map(Number).reduce((s, v) => s + v, 0);
    const name = this.state.service.map(ser => ser.creator).map(name => name.name);

    const columns = [
      {
        Header: '#',
        id: 'row',
        maxWidth: 50,
        filterable: false,
        Cell: row => <div>{row.index + 1}</div>,
        style: {
          textAlign: 'center',
        },
      },
      {
        Header: 'Create By',
        accessor: 'creator.name',
        filterable: false,
        sortable: false,
      },
      {
        Header: 'Totalprice',
        accessor: 'totalprice',
        filterable: false,
        sortable: false,
      },
      {
        Header: 'Generation',
        accessor: 'generation',
        filterable: false,
        sortable: false,
      },
      {
        id: 'haircut',
        Header: 'HairCut',
        accessor: d => d.haircut.toString(),
        filterable: false,
        sortable: false,
      },
      {
        id: 'shave',
        Header: 'Shave',
        accessor: d => d.shave.toString(),
        filterable: false,
        sortable: false,
      },
      {
        id: 'delet',
        Header: 'Remove',
        accessor: '_id',
        maxWidth: 100,
        Cell: ({value}) => (
          <button 
            className="btn btn-danger btn-sm" 
            onClick={(e)=>{ this.delServer(e,{
              id: value
            }) }}>Remove</button>),
        filterable: false,
        sortable: false,
      }
    ];

    // Time now 
    const dateToFormat = new Date();

    return (
      <React.Fragment>
        <Styles>
          <Line subTitle="Member List Service" stylez="style-eight" />
          <Container>
            <p className="text-date">
              Date :{" "}
              <Moment format="DD/MM/YYYY">{startOfMonth}</Moment>
              {" - "}
              <Moment format="DD/MM/YYYY">{dateToFormat}</Moment>
            </p>
            <p className="create-by">
              Create by :{" "}
              {name[0] ? name[0] : '---'}
            </p>
            <div className="total-price">
              <h4>Total price : {total ? total : 0}</h4>
            </div>
            {this.state.isLoading && <Loading />}
              <ReactTable style={{ textAlign: 'center' }}
                data={service}
                columns={columns}
                filterable
                defaultPageSize={5}
                pageSizeOptions={[10, 25, 50, 100, 150, 250]} />
          </Container>
        </Styles>
      </React.Fragment>
    );
  }
}

export default ListServiceAdmin;
