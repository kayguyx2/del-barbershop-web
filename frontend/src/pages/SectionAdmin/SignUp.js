import React from 'react';
import { Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import ErrorModal from '../../components/Modal/ErrorModal/ErrorModal';
import SuccesModal from '../../components/Modal/SuccessModal/SuccessModal';
import Loading from '../../components/Loading/loading';
import styled from 'styled-components';
import iconAdmin from '../../assets/icon/admin.png';


const AppWrapper = styled.div`  
    display: flex ;
    justify-content: center ;
    font-family: 'Roboto', sans-serif;
    color: #212529;
`;



const ProgressBarContainer = styled.div`
    width: 60%;  
    margin-top: 4%;
    text-align: left;
`;

const SingUpCon = styled.div`
    width: 100%;
    padding: 10%;
    box-shadow: 7px 7px 21px 5px rgba(0,0,0,0.21);
    border-radius: 15px;
`;

const Icon = styled.div`
    text-align: center;
`;

const Title = styled.div`
    margin-top: 15px;
    margin-bottom: 30px;
    font-family: 'Roboto', sans-serif;
    text-align: center;
`;
export default class Example extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            imageUrl: null
        };

        this.nameEl = React.createRef(); 
        this.phoneEl = React.createRef();
        this.emailEl = React.createRef();
        this.contactEl = React.createRef();
        this.contentEl = React.createRef();
        this.passwordEl = React.createRef();
        this.passwordEl2 = React.createRef();
    }
    
    targerValue = event =>{
        this.setState({
            imageUrl: event.target.files[0]
        })
    }
    render() {
        console.log("img : ",this.state.imageUrl);
        
        return (
            <AppWrapper>
                <ProgressBarContainer>
                <SingUpCon>
                {this.props.error &&
                    <ErrorModal
                        message={this.props.error.message}
                        close={this.props.errorHandler}
                    />
                }
                {this.props.success &&
                <SuccesModal
                close={this.props.successHandler}
                />}
                {this.props.loading && <Loading />}
                {!this.props.loading &&
                <React.Fragment>
                    <Icon>
                    <img
                        className="img-responsive"
                        src={iconAdmin}
                        width={120}
                        height={90}
                        alt="icon." />
                </Icon>
                <Title>
                    <h2>Sing Up Admin</h2>
                </Title>
                <Form 
                    onSubmit={e => this.props.onSignup(e, {
                        name: this.nameEl.value,
                        phone: this.phoneEl.value,
                        email: this.emailEl.value,
                        imageUrl: this.state.imageUrl,
                        contact: this.contactEl.value,
                        content: this.contentEl.value,
                        password: this.passwordEl.value,
                        confirm_password: this.passwordEl2.value,
                })
                }
                >
                    <FormGroup row>
                        <Label for="Email" sm={3}>Email</Label>
                        <Col sm={9}>
                            <Input 
                                type="email" 
                                name="email" 
                                id="Email" 
                                placeholder="input e-mail"
                                innerRef={input => (this.emailEl = input)}
                                />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="Password" sm={3}>Password</Label>
                        <Col sm={9}>
                            <Input 
                                type="password" 
                                name="password" 
                                id="Password" 
                                autoComplete="off"
                                placeholder="input password"
                                innerRef={input => (this.passwordEl = input)}
                                />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="Confirm-Password" sm={3} >Confirm Password</Label>
                        <Col sm={9}>
                            <Input 
                                type="password" 
                                name="password" 
                                autoComplete="off"
                                id="Confirm-Password" 
                                placeholder="input confirm password"
                                innerRef={input => (this.passwordEl2 = input)}
                                />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="Name" sm={3} >Name</Label>
                        <Col sm={9}>
                            <Input 
                                type="text" 
                                name="name" 
                                id="Name" 
                                placeholder="input name"
                                innerRef={input => (this.nameEl = input)}
                                />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="Contact" sm={3} >Contact</Label>
                        <Col sm={9}>
                            <Input 
                                type="text" 
                                name="contact" 
                                id="Contact" 
                                placeholder="input facebook or IG "
                                innerRef={input => (this.contactEl = input)}
                                />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="Phone" sm={3} >Phone</Label>
                        <Col sm={9}>
                            <Input 
                                type="text" 
                                name="phone" 
                                id="Phone" 
                                placeholder="input phonenumber"
                                innerRef={input => (this.phoneEl = input)}
                                />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="Text" sm={3}>Content</Label>
                        <Col sm={9}>
                        <Input 
                            type="textarea" 
                            name="text" 
                            id="Text"
                            innerRef={input => (this.contentEl = input)}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="exampleFile" sm={3}>File</Label>
                        <Col sm={9}>
                        <Input 
                            type="file" 
                            name="file" 
                            id="exampleFile" 
                            onChange={this.targerValue}
                            />
                        </Col>
                    </FormGroup>
                        <Col sm={{ size: 2, offset: 10 }}>
                            <Button type="submit" >Submit</Button>
                        </Col>
                </Form>
                </React.Fragment>
                
                }
                </SingUpCon>
                </ProgressBarContainer>
            </AppWrapper>
        
        );
  }
}