import React from 'react';
import {
  Container,
  Row,
  Col,
  Image,
  Button,
  InputGroup,
  FormControl,
} from 'react-bootstrap';
import styled from 'styled-components';
import img from '../../assets/test1.jpg';

const Styles = styled.div`
  .title {
    margin-top: 25px;
    display: inline-block;
    position: relative;
    text-align: center;
  }
  .title-ad {
    margin-top: 10px;
    font-family: Roboto;
  }
  .btn-change {
    margin-top: 20px;
  }
  .btn-comfirm {
    float: right;
  }
  .portfolio-ad {
    margin-top: 50px;
    font-family: Roboto;
  }
  .img {
    box-shadow: 0 16px 24px 2px rgba(0, 0, 0, 0.14),
      0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2);
    border-radius: 30px;
    margin-top: 10px;
    display: block;
    margin-left: auto;
    margin-right: auto;
  }
`;

export const MainAdmin = () => (
  <Styles>
    <div className="title">
      <h2>Main AdminPage Page</h2>
      <hr />
    </div>
    <Container>
      <Row>
        <Col xs={6} md={3}>
          <Image
            className="img"
            src={img}
            width={171}
            height={180}
            alt="171x180"
          />
          <Button className="btn-change" variant="outline-warning" block>
            Change Photo
          </Button>
        </Col>
        <Col xs={10} md={6}>
          <div className="title-ad">
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="inputGroup-sizing-default">
                  Name
                </InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl placeholder="Scalet" />
            </InputGroup>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="inputGroup-sizing-default">
                  Phon Number
                </InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl placeholder="099-3181995" />
            </InputGroup>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="inputGroup-sizing-default">
                  Contact
                </InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl placeholder="LINE :Scalet500" />
            </InputGroup>
          </div>
          <Button className="btn-comfirm" variant="info">
            Change infomation
          </Button>
        </Col>
      </Row>
      <div className="portfolio-ad">
        <hr />
        <h3>portfolio</h3>
      </div>
    </Container>
  </Styles>
);
