import React, { Component } from 'react'
import Loading from '../../components/Loading/loading';
import { Container, Row, Col } from 'react-bootstrap';
import moment from 'moment';
import styled from 'styled-components';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import '../../style/table.css';

//call class 
import Line from '../../components/HomePage/LineSubTitie/LineSubtitile';

const StyledBtn = styled.div`
    margin: 25px 0 ;  

    .btn-form{
        background-color: rgba(0,0,0,0.3);
        color: #f5f5f5;

        &:hover{
            background-color: rgba(0,0,0,0.7);
            color: rgba(0,0,0,0.8);
        }
    }
    .btn-act{
        background-color: #f5f5f5;
        color: rgba(0,0,0,0.8);
    }
`;

class ListBookAdmin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            event: [],
            date: [],
            isLoading: false,
            clickAll: false
        }
        this.selectBookProgress = this.selectBookProgress.bind(this);
        this.selectAllBooked = this.selectAllBooked.bind(this);
    }

    componentDidMount() {
        this.fetchService();
    }
    selectBookProgress() {
        const event = this.state.event;
        const dataToDay = event.filter(ser => ser.date === moment().format('L'))

        this.setState({
            data: dataToDay,
            clickAll: false
        })

    }
    selectAllBooked() {
        const event = this.state.event;

        this.setState({
            data: event,
            clickAll: true
        })

    }
    fetchService() {

        this.setState({ isLoading: true });
        const requestData = {
            query: `
                query{
                    events{
                    date
                    time
                    name
                    phone
                    key
                    barber{
                        _id
                        name
                    } 
                    }
                }
                `,
        };
        fetch('http://localhost:8000/graphql', {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${this.props.token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestData),
        })
            .then(res => {
                if (res.status !== 200 && res.status !== 201) {
                    throw new Error('Failed!');
                }
                return res.json();
            })
            .then(resData => {

                this.setState({
                    event: resData.data.events.filter(ser => ser.barber._id === this.props.userId).map(e => {
                        return {
                            ...e,
                            date: moment(e.date).format('L'),
                            time: moment(e.time).format('LT')
                        }
                    }),
                    isLoading: false
                }, this.selectBookProgress);
            })
            .catch(err => {
                console.log(err);
                this.setState({ isLoading: false });

            });
    }

    render() {
        const columns = [
            {
                Header: '#',
                id: 'row',
                maxWidth: 50,
                filterable: false,
                Cell: row => <div>{row.index + 1}</div>,
                style: {
                    textAlign: 'center',
                },
            },
            {
                Header: 'Key',
                accessor: 'key',
                filterable: false,
                sortable: false,
            },
            {
                Header: 'Customer Name',
                accessor: 'name',
                filterable: false,
                sortable: false,
            },
            {
                Header: 'Customer Phone',
                accessor: 'phone',
                filterable: false,
                sortable: false,
            },
            {
                Header: 'Date',
                accessor: 'date',
                filterable: false,
                sortable: false,
            },
            {
                Header: 'Time',
                accessor: 'time',
                filterable: false,
                sortable: false,
            }
        ];

        return (

            <div>
                <Line subTitle="Booked List Service" stylez="style-eight" />
                <Container >
                    <StyledBtn>
                        <Row >
                            <Col sm={12} md={6} >
                                <button
                                    className={this.state.clickAll ? "btn btn-form":"btn btn-act"}
                                    onClick={this.selectBookProgress}>
                                    Booking in progress
                                </button>
                            </Col>
                        </Row>
                        <br />
                        <Row >
                            <Col sm={12} md={6} >
                                <button
                                    className={this.state.clickAll ? "btn btn-act":"btn btn-form"}
                                    onClick={this.selectAllBooked}>
                                    Booking has been already
                                </button>
                            </Col>
                        </Row>
                    </StyledBtn>
                    {this.state.isLoading && <Loading />}

                    <ReactTable style={{ textAlign: 'center' }}
                        data={this.state.data}
                        columns={columns}
                        filterable
                        defaultPageSize={5}
                        pageSizeOptions={[10, 25, 50, 100, 150, 250]} />
                </Container>
            </div>
        )
    }
}

export default ListBookAdmin