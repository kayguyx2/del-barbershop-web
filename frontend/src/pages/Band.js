import React, { Component } from 'react'
import { Container, Image } from 'react-bootstrap';
class Band extends Component {
    componentDidMount() {
        this.props.autoBandLogout();
    }
    render() {
        return (
            <Container>
                <div style={{textAlign: "center"}}>
                    <Image
                        src={'http://localhost:8000/images/icon/Block.png'}
                        rounded 
                        style={{
                            maxHeight: '170px',
                            width: 'auto',
                            marginTop: '70px'
                        }} />
                    <br />
                    <br />
                    <h2>Blocked User ID</h2>
                    <h4>You have violated the barber shop's rules. Or didn't come by appointment</h4>
                    <h4>Please contact the barber shop or call - 080 845 4381.</h4>
                    <br />
                    <h5 style={{color: 'red'}}>system auto logout in 10 seconds</h5>
                </div>

            </Container>
        )
    }
}
export default Band